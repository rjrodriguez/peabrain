--- /home/ivan/Downloads/pipe421original/src/pipe/actions/FileAction.java	2012-08-02 08:14:18.000000000 +0200
+++ /home/ivan/PeabraiN421/src/pipe/actions/FileAction.java	2014-04-24 21:10:19.410074251 +0200
@@ -7,6 +7,7 @@
 import pipe.views.PipeApplicationView;
 import pipe.gui.widgets.FileBrowser;
 import pipe.models.PipeApplicationModel;
+import pipe.modules.bounds.lpp.solvers.solverSelector.*;
 
 import javax.swing.*;
 import java.awt.event.ActionEvent;
@@ -70,6 +78,12 @@
             {
                 pipeApplicationView.createNewTab(null, false); // Create a new tab
             }
+            else if((this == pipeApplicationModel.settingsAction))
+            {
+                JFrame SolverSelectorGUI = new SolverSelectorGUI();
+                SolverSelectorGUI.setVisible(true);
+                SolverSelectorGUI.pack();
+            }
             else if((this == pipeApplicationModel.exitAction) && pipeApplicationView.checkForSaveAll())
             {
                 pipeApplicationView.dispose();
