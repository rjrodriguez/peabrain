--- /home/ivan/Downloads/pipe421original/src/pipe/views/PetriNetView.java	2011-09-14 03:08:12.000000000 +0200
+++ /home/ivan/PeabraiN421/src/pipe/views/PetriNetView.java	2013-02-10 10:56:33.000000000 +0100
@@ -53,12 +53,13 @@
     private PetriNetController _petriNetController;
     private PetriNet _model;
 
-
-    public PetriNetView(String pnmlFileName)
+    public PetriNetView(String pnmlFileName, boolean isGUI)
+    //public PetriNetView(String pnmlFileName)
     {
         _tokenViews = null;
         _model = new PetriNet();
-        _petriNetController = ApplicationSettings.getPetriNetController();
+        if(isGUI)
+        	_petriNetController = ApplicationSettings.getPetriNetController();
         _model.registerObserver(this);
         initializeMatrices();
         PNMLTransformer transform = new PNMLTransformer();
@@ -66,6 +67,10 @@
         _model.setPnmlName(temp.getName());
         createFromPNML(transform.transformPNML(pnmlFileName));
     }
+    
+    public PetriNetView(String pnmlFileName){
+    	this(pnmlFileName,true);
+    }
 
     public PetriNetView(PetriNetController petriNetController, PetriNet model)
     {
