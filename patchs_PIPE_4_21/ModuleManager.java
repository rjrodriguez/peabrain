--- /home/ivan/Downloads/pipe421original/src/pipe/gui/ModuleManager.java	2011-09-08 19:30:52.000000000 +0200
+++ /home/ivan/PeabraiN421/src/pipe/gui/ModuleManager.java	2013-05-19 12:47:30.000000000 +0200
@@ -16,6 +16,7 @@
 import java.lang.reflect.Method;
 import java.net.URL;
 import java.util.ArrayList;
+import java.util.Arrays;
 import java.util.HashSet;
 import java.util.Iterator;
 import java.util.Vector;
@@ -46,6 +47,8 @@
     private JTree moduleTree;
     private DefaultTreeModel treeModel;
     private DefaultMutableTreeNode load_modules;
+    private DefaultMutableTreeNode load_modulesPeabraiN;
     private final String loadNodeString = "Find IModule";
     private final Component parent;
 
@@ -138,6 +141,18 @@
      */
     private void addClassToTree(Class moduleClass)
     {
+		//XXX This should be done in some dynamic way
+       	String PeabraiN[] =  {"Structural Marking Bound",
+							"Linear Bound",
+       						"Visit Ratios Computation",
+       						"Performance Estimation",
+       						"Resource Optimisation",
+       						"Structural Enabling Bound",
+       						"Structural Implicit Places",
+       						"GSPN Simulation Analysis",
+				};
+       	
         DefaultMutableTreeNode modNode;
         if(installedModules.add(moduleClass))
         {
@@ -163,7 +178,14 @@
             {
                 Object m = ((DefaultMutableTreeNode) modNode.getFirstChild()).
                         getUserObject();
-                load_modules.add(new DefaultMutableTreeNode(m));
+                if (Arrays.asList(PeabraiN).contains(modNode.getFirstChild().toString())){
+                	load_modulesPeabraiN.add(new DefaultMutableTreeNode(m));
+                }else{
+                	load_modules.add(new DefaultMutableTreeNode(m));
+                }
             }
             else load_modules.add(modNode);
         }
@@ -215,8 +237,12 @@
         DefaultMutableTreeNode root = new DefaultMutableTreeNode("Analysis Module Manager");
 
         // create root children
-        load_modules = new DefaultMutableTreeNode("Available Modules");
+        //load_modules = new DefaultMutableTreeNode("Available Modules");
+        load_modules = new DefaultMutableTreeNode("PIPE Available Modules");
+        load_modulesPeabraiN = new DefaultMutableTreeNode("PeabraiN Available Modules");
 
+        // Find iModules
         DefaultMutableTreeNode add_modules = new DefaultMutableTreeNode(loadNodeString);
 
         // iterate over the class names and create a node for each
@@ -236,6 +262,8 @@
         }
 
         root.add(load_modules);
+        root.add(load_modulesPeabraiN);
         root.add(add_modules);
 
         treeModel = new DefaultTreeModel(root);
