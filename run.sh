#!/bin/bash
# Script for running PeabraiN

# Set this variable properly (set to installation directory by default)
JNIDIR=/usr/lib/jni

# Run PeabraiN with PIPE
java -cp bin/lib/*:bin/. -Djava.library.path=$JNIDIR Pipe
