/**
 * Filename: ResourceOptimisationGUI.java
 * Date: January, 2012 -- first release
 * Creates a GUI for collecting information needed (epsilon) for computing
 * the better resource distribution for a given budget on a Petri net model
 * 
 * See ResourceOptimisationStrategy.java
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JScrollPane;

import pipe.modules.bounds.errors.ErrorConstants;
import pipe.modules.bounds.errors.ErrorMessage;
import pipe.modules.bounds.strategies.ResourceOptimisationStrategy;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ResourceOptimisationGUI extends PeabraiNGUI {
	private static final long serialVersionUID = 3929568015027796573L;

	private JComboBox comboBox;

	private JScrollPane scrollPane_2;
	private JFormattedTextField txtBudget;
	private JTable table;
	private MyTableModel tableModel;
	

	private String[] getItemsData() {
		String[] vStrings = null;
		
		ArrayList<Integer> m0 = this.pnModel.getPlacesMarkedAtInitialMarking();
		vStrings = new String[m0.size()];
		for(int i = 0; i < vStrings.length; i++)
			vStrings[i] = this.pnModel.getPlaceID(((Integer)m0.get(i)).intValue());

		return vStrings;
	}
	
	private String[] getColumnsName()
	{
		String[] aux = {"Resource", "Unit cost"};
		return aux;
	}
	
	
	private Object[][] getTableData()
	{
		Object[][] aux = null;
		try{
			ArrayList<Integer> m0 = this.pnModel.getPlacesMarkedAtInitialMarking();
			aux = new Object[m0.size() - 1][2];
			
			int idx = 0;
			for(int i = 0; i < m0.size(); i++)
			{
				if(this.pnModel.getPlaceID(((Integer)m0.get(i)).intValue()).equals((String)comboBox.getSelectedItem())) // Avoid selected item
				{
					idx = 1;
					continue;
				}
				else
				{
					aux[i - idx][0] = this.pnModel.getPlaceID((int)m0.get(i));
					aux[i - idx][1] = "1000"; // Initial budget: $1000
				}
			}
		}
		catch(Exception e){
			
		}
		return aux;
	}
	
	public ResourceOptimisationGUI(ResourceOptimisationStrategy ros)
	{
		super(ros.getCurrentPNModel(), true, ros);
	}

	protected void enableComputeButton() {
		this.btnCompute.setEnabled(!(pnModel.getPlacesSize() == 0 || pnModel.getTransitionsSize() == 0) && 
				txtBudget.getText().length() > 0);
		
		/* Reset data in the table, depending on the new place selected */
		tableModel.setDataVector(getTableData(), getColumnsName());
	}

	protected void compute()
	{
		/* Get table data */
		int[] idxResources = new int[tableModel.getRowCount()];
		double[] vCost = new double[idxResources.length];
		for(int i = 0; i < idxResources.length; i++)
		{
			idxResources[i] = this.pnModel.getPlaceIndex((String) tableModel.getValueAt(i, 0));
			vCost[i] = Double.parseDouble((String) tableModel.getValueAt(i, 1));
		}
		
		double budget = -1;
		try
		{
			budget = Double.parseDouble(txtBudget.getText());
		}
		catch(NumberFormatException e)
		{
			e.printStackTrace();
		}
		if(budget < 0)
		{
			ErrorMessage.showErrorMessage(ErrorConstants.NO_BUDGET_GIVEN);
		}
		else
		{
			/* Create ResourceOptStrategy and compute it... */
			((ResourceOptimisationStrategy) this.strategy).
				setParametersBudget(budget);
			((ResourceOptimisationStrategy) this.strategy).
				setParametersCostVector(vCost);
			((ResourceOptimisationStrategy) this.strategy).
				setParametersIdxResources(idxResources);
			((ResourceOptimisationStrategy) this.strategy).
				setParametersProcessIdle((String)comboBox.getSelectedItem());
	
			strategy.compute();
			
			/* Get and show results */
			setText(strategy.printResult());
		}
	}
	
	
	
	private class MyTableModel extends DefaultTableModel
	{
		private static final long serialVersionUID = 6739152974904569148L;

		public boolean isCellEditable (int row, int column)
		   {
		       if (column != 0) // Cell 0 not editable
		          return true;
		       else
		    	   return false;
		   }
	}

	@Override
	protected void createAdditionalGUIElements() {
		pnlData.setPreferredSize(new Dimension(450, 100));
		pnlData.setLayout(null);
		
		comboBox = new JComboBox(getItemsData());
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enableComputeButton();
			}
		});
		comboBox.setBounds(151, 5, 80, 24);
		pnlData.add(comboBox);
		
			
		JLabel lblProcessidlePlace = new JLabel("Process-idle place:");
		lblProcessidlePlace.setBounds(12, 10, 149, 15);
		pnlData.add(lblProcessidlePlace);
		
		JLabel lblBudget = new JLabel("Budget:");
		lblBudget.setBounds(12, 37, 70, 15);
		pnlData.add(lblBudget);
		
		
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		DecimalFormat format = new DecimalFormat("#.####");
		format.setDecimalFormatSymbols(symbols);
		
		txtBudget = new JFormattedTextField(format);
		txtBudget.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				enableComputeButton();
			}
		});
		txtBudget.setBounds(151, 37, 80, 19);
		pnlData.add(txtBudget);
		txtBudget.setColumns(10);
		txtBudget.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enableComputeButton();
			}
		});
		
		table = new JTable(tableModel = new MyTableModel());
		tableModel.setDataVector(getTableData(), getColumnsName());
		//table.setBounds(12, 87, 219, 128);
		//getContentPane().add(table);
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(243, 0, 195, 99);
		pnlData.add(scrollPane_2);
		scrollPane_2.setViewportView(table);
		
		JLabel lblResourceCosts = new JLabel("Resource costs:");
		lblResourceCosts.setBounds(12, 64, 123, 15);
		pnlData.add(lblResourceCosts);
	}

	@Override
	protected String getGUIName() {
		return "Resource Optimisation";
	}
}
