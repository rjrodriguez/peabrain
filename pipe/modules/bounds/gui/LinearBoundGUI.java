/**
 * Filename: LinearBoundGUI.java
 * Date: January, 2012 -- first release
 * Creates a GUI for collecting information needed for computing
 * (lower, upper) performance bounds for a Petri net model
 *  
 *  See GeneralBound.java, LowerThroughputBound.java, UpperThroughputBound.java
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.gui;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;

import pipe.modules.bounds.strategies.LinearBoundStrategy;
import pipe.modules.bounds.strategies.LinearBoundStrategy2;

import javax.swing.JCheckBox;
import java.awt.Dimension;


public class LinearBoundGUI extends PeabraiNGUI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2097468150781191866L;
	private JComboBox cmbBoxLowerBound, cmbBoxUpperBound, cmbBoxUpperAvgMarking;
	private JCheckBox chckbxSlowest, chckbxUpperThrBound, chckbxLowerThrBound, chkBoxUpperAverageMarking;	
	private JCheckBox chkBoxLowerAverageMarking;
	private JComboBox cmbBoxLowerAvgMarking;
	
	private String[] getTransitionsData() {
		/* Create a string for each place/transition */
		
		try{
			String[] vStrings = new String[this.pnModel.getTransitionsSize()];
			
			for(int i = 0; i < vStrings.length; i++)
				vStrings[i] = this.pnModel.getTransitionID(i);
	
			return vStrings;
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	private String[] getPlacesData() {
		/* Create a string for each place/transition */
		
		try{
			String[] vStrings = new String[this.pnModel.getPlacesSize()];
			
			for(int i = 0; i < vStrings.length; i++)
				vStrings[i] = this.pnModel.getPlaceID(i);
	
			return vStrings;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	public LinearBoundGUI(LinearBoundStrategy2 lbs)
	{
		super(lbs.getCurrentPNModel(), true, lbs);
		btnCompute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
	}

	protected void enableComputeButton() {
		this.btnCompute.setEnabled(
				(!(pnModel.getPlacesSize() == 0 || pnModel.getTransitionsSize() == 0) && 
					(this.chckbxLowerThrBound.isSelected() || 
							this.chckbxUpperThrBound.isSelected() || 
							this.chckbxSlowest.isSelected() || 
							this.chkBoxUpperAverageMarking.isSelected() ||
							this.chkBoxLowerAverageMarking.isSelected()
							)));
		
	}

	protected void compute()
	{
		/* Disable button */
		btnCompute.setEnabled(false);

		/* Set parameters */
		((LinearBoundStrategy2)this.strategy).
			setParametersForLowerBoundComputation(
						this.chckbxLowerThrBound.isSelected(), this.cmbBoxLowerBound.getSelectedIndex());
		((LinearBoundStrategy2)this.strategy).
			setParametersForUpperBoundComputation(
					this.chckbxUpperThrBound.isSelected(), this.cmbBoxUpperBound.getSelectedIndex());
		((LinearBoundStrategy2)this.strategy).
			setParametersForSlowestBoundComputation(this.chckbxSlowest.isSelected());
		//((LinearBoundStrategy)this.strategy).
		//setParametersForLowerAverageMarkingComputation(this.chkBoxLowerAverageMarking.isSelected(), this.cmbBoxLowerAvgMarking.getSelectedIndex());
		//((LinearBoundStrategy)this.strategy).
		//setParametersForUpperAverageMarkingComputation(this.chkBoxUpperAverageMarking.isSelected(), this.cmbBoxUpperAvgMarking.getSelectedIndex());
		
		
		/* Get bounds */
		this.strategy.compute();
		
		/* Get and show results */
		setText(this.strategy.printResult());
	}

	@Override
	protected void createAdditionalGUIElements()
	{
		pnlData.setPreferredSize(new Dimension(375, 155));
		
		chckbxUpperThrBound = new JCheckBox("Upper thr. bound for:");
		
				cmbBoxLowerBound = new JComboBox(getTransitionsData());
				cmbBoxLowerBound.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				
				chckbxSlowest = new JCheckBox("Compute also the slowest p-semiflow");
				cmbBoxUpperBound = new JComboBox(getTransitionsData());
				cmbBoxUpperBound.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				chckbxLowerThrBound = new JCheckBox("Lower thr. bound for:");
				
				chkBoxUpperAverageMarking = new JCheckBox("Upper avg. marking bound for:");
				
				cmbBoxUpperAvgMarking = new JComboBox(getPlacesData());
				cmbBoxUpperAvgMarking.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				
				chkBoxLowerAverageMarking = new JCheckBox("Lower avg. marking bound for:");
				
				cmbBoxLowerAvgMarking = new JComboBox(getPlacesData());
				cmbBoxLowerAvgMarking.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				GroupLayout gl_pnlData = new GroupLayout(pnlData);
				gl_pnlData.setHorizontalGroup(
					gl_pnlData.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlData.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnlData.createSequentialGroup()
									.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING)
										.addComponent(chckbxLowerThrBound)
										.addComponent(chckbxUpperThrBound))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING)
										.addComponent(cmbBoxLowerBound, 0, 123, Short.MAX_VALUE)
										.addComponent(cmbBoxUpperBound, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
									.addGap(84))
								.addGroup(gl_pnlData.createSequentialGroup()
									.addComponent(chckbxSlowest)
									.addContainerGap(76, Short.MAX_VALUE))
								.addGroup(gl_pnlData.createSequentialGroup()
									.addComponent(chkBoxLowerAverageMarking)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(cmbBoxLowerAvgMarking, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
									.addContainerGap())
								.addGroup(gl_pnlData.createSequentialGroup()
									.addComponent(chkBoxUpperAverageMarking)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(cmbBoxUpperAvgMarking, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
									.addContainerGap(15, Short.MAX_VALUE))))
				);
				gl_pnlData.setVerticalGroup(
					gl_pnlData.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlData.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_pnlData.createParallelGroup(Alignment.BASELINE)
								.addComponent(chckbxLowerThrBound)
								.addComponent(cmbBoxLowerBound, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING)
								.addComponent(chckbxUpperThrBound)
								.addComponent(cmbBoxUpperBound, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(chckbxSlowest)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_pnlData.createParallelGroup(Alignment.BASELINE)
								.addComponent(chkBoxLowerAverageMarking)
								.addComponent(cmbBoxLowerAvgMarking, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING)
								.addComponent(cmbBoxUpperAvgMarking, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(chkBoxUpperAverageMarking))
							.addGap(1))
				);
				pnlData.setLayout(gl_pnlData);
				chckbxLowerThrBound.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				chkBoxUpperAverageMarking.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				chkBoxLowerAverageMarking.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				chckbxSlowest.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				cmbBoxLowerBound.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
		chckbxUpperThrBound.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enableComputeButton();
			}
		});
	}

	@Override
	protected String getGUIName() {
		return "Average Throughput and Marking Computation";
	}
}
