/**
 * Filename: PerformanceEstimationGUI.java
 * Date: January, 2012 -- first release
 * Creates a GUI for collecting information needed (epsilon) for computing
 * improved performance bounds for a Petri net model
 * 
 * See PerformanceEstimationStrategy.java
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.gui;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;


import pipe.modules.bounds.strategies.PerformanceEstimationStrategy;

import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class PerformanceEstimationGUI extends PeabraiNGUI {
	private static final long serialVersionUID = 6018748852760669648L;
	
	private JTextField txtEpsilon;
	
	public PerformanceEstimationGUI(PerformanceEstimationStrategy pes)
	{
		super(pes.getCurrentPNModel(), true, pes);
	}
	
	protected void enableComputeButton() {
		this.btnCompute.setEnabled(!(pnModel.getPlacesSize() == 0 || pnModel.getTransitionsSize() == 0) && 
				txtEpsilon.getText().length() > 0);
	}
	
	protected void compute()
	{
		/* Disable button */
		btnCompute.setEnabled(false);
		
		/* Set parameter and compute it... */
		double epsilon = Double.parseDouble((txtEpsilon.getText())); 
		
		((PerformanceEstimationStrategy)strategy).
			setParameterEpsilon(epsilon);
		strategy.compute();
		
		/* Get and show results */
		setText(strategy.printResult());
	}

	@Override
	protected void createAdditionalGUIElements() {
		pnlData.setPreferredSize(new Dimension(175, 75));
		pnlData.setLayout(null);

		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		DecimalFormat format = new DecimalFormat("#.####");
		format.setDecimalFormatSymbols(symbols);
		txtEpsilon = new JFormattedTextField(format);
		txtEpsilon.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				enableComputeButton();
			}
		});
		txtEpsilon.setBounds(73, 12, 79, 19);
		pnlData.add(txtEpsilon);
		txtEpsilon.setColumns(10);
		
		JLabel lblEpsilon = new JLabel("Epsilon:");
		lblEpsilon.setBounds(12, 14, 70, 15);
		pnlData.add(lblEpsilon);
	}

	@Override
	protected String getGUIName() {
		return "Performance Estimation Computation";
	}
}
