
package pipe.modules.bounds.gui;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;

import pipe.modules.bounds.strategies.MarkingUpperBoundStrategy;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class MarkingUpperBoundGUI extends PeabraiNGUI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2097468150781191866L;
	private JComboBox cmbBoxUpperBound;
	private JComboBox cmbBoxPlaces;
	private JTextField txtThrBound;
	
	private String[] getTransitionsData() {
		/* Create a string for each place/transition */
		
		try{
			String[] vStrings = new String[this.pnModel.getTransitionsSize()];
			
			for(int i = 0; i < vStrings.length; i++)
				vStrings[i] = this.pnModel.getTransitionID(i);
	
			return vStrings;
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	private String[] getPlacesData() {
		/* Create a string for each place/transition */
		
		try{
			String[] vStrings = new String[this.pnModel.getPlacesSize()];
			
			for(int i = 0; i < vStrings.length; i++)
				vStrings[i] = this.pnModel.getPlaceID(i);
	
			return vStrings;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	public MarkingUpperBoundGUI(MarkingUpperBoundStrategy lbs)
	{
		super(lbs.getCurrentPNModel(), true, lbs);
		btnCompute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
	}

	protected void enableComputeButton() {
		this.btnCompute.setEnabled(
				(!(pnModel.getPlacesSize() == 0 || pnModel.getTransitionsSize() == 0)));
		
	}

	protected void compute()
	{
		/* Disable button */
		btnCompute.setEnabled(false);

		/* Set values */
		((MarkingUpperBoundStrategy)this.strategy).setPlace(this.cmbBoxPlaces.getSelectedIndex());
		((MarkingUpperBoundStrategy)this.strategy).setTransition(this.cmbBoxUpperBound.getSelectedIndex());
		((MarkingUpperBoundStrategy)this.strategy).setThrBoundValue(Double.parseDouble(this.txtThrBound.getText()));
		
		/* Get bounds */
		this.strategy.compute();
		
		/* Get and show results */
		setText(this.strategy.printResult());
	}

	@Override
	protected void createAdditionalGUIElements()
	{
		pnlData.setPreferredSize(new Dimension(375, 100));
				cmbBoxUpperBound = new JComboBox(getTransitionsData());
				cmbBoxUpperBound.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				
				JLabel lblUpperThrBound = new JLabel("Upper thr. bound for:");
				
				txtThrBound = new JTextField();
				txtThrBound.setColumns(10);
				
				cmbBoxPlaces = new JComboBox(getPlacesData());
				cmbBoxPlaces.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableComputeButton();
					}
				});
				
				JLabel lblPlaceToIncrement = new JLabel("Place to increment tokens:");
				GroupLayout gl_pnlData = new GroupLayout(pnlData);
				gl_pnlData.setHorizontalGroup(
					gl_pnlData.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlData.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING)
								.addComponent(lblPlaceToIncrement)
								.addComponent(lblUpperThrBound))
							.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_pnlData.createSequentialGroup()
									.addGap(3)
									.addComponent(cmbBoxPlaces, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
									.addGap(65))
								.addGroup(gl_pnlData.createSequentialGroup()
									.addGap(5)
									.addComponent(cmbBoxUpperBound, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(txtThrBound, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
									.addGap(4))))
				);
				gl_pnlData.setVerticalGroup(
					gl_pnlData.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlData.createSequentialGroup()
							.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_pnlData.createSequentialGroup()
									.addContainerGap()
									.addGroup(gl_pnlData.createParallelGroup(Alignment.LEADING)
										.addComponent(lblUpperThrBound)
										.addComponent(cmbBoxUpperBound, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.UNRELATED))
								.addGroup(Alignment.TRAILING, gl_pnlData.createSequentialGroup()
									.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(txtThrBound, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
									.addGap(6)))
							.addGroup(gl_pnlData.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblPlaceToIncrement)
								.addComponent(cmbBoxPlaces, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addContainerGap(30, Short.MAX_VALUE))
				);
				pnlData.setLayout(gl_pnlData);
	}

	@Override
	protected String getGUIName() {
		return "Marking Upper Bound Computation";
	}
}
