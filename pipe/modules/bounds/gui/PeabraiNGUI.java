/**
 * Filename: PeabraiNGUI.java
 * Date: January, 2012 -- first release
 * Abstract class for being superclass of each GUI used by strategies.
 * Defines abstract methods for being implemented by subclasses, related
 * to the action performed by the 'Compute' button (common widget)
 * and the conditions to be enabled.
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import pipe.gui.widgets.ResultsHTMLPane;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.strategies.Strategy;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;

public abstract class PeabraiNGUI extends JFrame {
	private static final long serialVersionUID = 8725962462587561276L;
	
	protected PetriNetModel pnModel;
	protected Strategy strategy;
	protected JEditorPane textArea;
	protected JButton btnCompute;

	protected JScrollPane scrollPane_1;
	protected JPanel pnlContent;
	protected JPanel pnlResults;
	protected JPanel pnlData;

	public PeabraiNGUI(PetriNetModel pnModel, 
						boolean createAdditionals,
						Strategy strategy)
	{
		this.pnModel = pnModel;
		this.strategy = strategy;
		getContentPane().setLayout(new BorderLayout(0, 0));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		pnlContent = new JPanel();
		getContentPane().add(pnlContent, BorderLayout.NORTH);
		
		
		pnlResults = new JPanel();
		getContentPane().add(pnlResults, BorderLayout.CENTER);
		
		btnCompute = new JButton("Compute");
		btnCompute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				compute();				
			}
		});
		btnCompute.setEnabled(false);
	
		pnlContent.setLayout(new BorderLayout(0, 0));
		pnlContent.add(btnCompute, BorderLayout.CENTER);
		
		pnlData = new JPanel();
		pnlContent.add(pnlData, BorderLayout.WEST);
		
		pnlResults.setLayout(new BorderLayout(0, 0));
		
		scrollPane_1 = new JScrollPane();
		pnlResults.add(scrollPane_1, BorderLayout.CENTER);
		pnlResults.setPreferredSize(new Dimension(500, 300));
		
		textArea = new JEditorPane();
		scrollPane_1.setViewportView(textArea);
		textArea.setEditable(false);
		textArea.setContentType("text/html");
		
		this.setTitle(getGUIName());

		if(createAdditionals)
		{
			createAdditionals();
		}
	}
	
	protected void createAdditionals() {
		createAdditionalGUIElements();
		// Center & show
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	protected abstract void createAdditionalGUIElements();
	
	/**
	 * Enables the 'Compute' button
	 * To be implemented by subclasses
	 */
	protected abstract void enableComputeButton();

	/**
	 * Method to be invoked when pressing the 'Compute' button
	 * To be implemented by each subclass
	 */
	protected abstract void compute();
	
	protected void setText(String text)
	{
		this.textArea.setText("<html><head>" + ResultsHTMLPane.HTML_STYLE + "</head><body>" + text +
	              "</body></html>");
		this.textArea.setCaretPosition(0); /* Scroll up */
	}
	
	protected abstract String getGUIName();

}
