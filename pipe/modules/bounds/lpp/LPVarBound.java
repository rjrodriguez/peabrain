/**
 * Filename: LPVarBound.java
 * Date: January, 2012 -- first release
 * Defines bound per LP variables
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.lpp;

public class LPVarBound {
	private String variable;
	private double value;
	
	/**
	 * Constructor for a bound. It need the variable, in String format, and the
	 * value to be set as bound (no distinguish at this level if it is upper or lower...)
	 * @param variable to be bounded
	 * @param value bound value
	 */
	public LPVarBound(String variable, double value)
	{
		this.variable = variable;
		this.value = value;
	}
	
	/**
	 * Gets the variable, in String format
	 * @return variable identification
	 */
	public String getVariable() {
		return variable;
	}

	/**
	 * Gets the bound value
	 * @return Bound value
	 */
	public double getValue() {
		return value;
	}
}
