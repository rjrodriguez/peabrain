/**
 * Filename: UpperAverageMarkingBound.java
 * Date: January, 2012 -- first release
 * Defines constraints for computing average marking of a place of a PN model
 * 
 *   m = m0 + C·\sigma (reachability)
 *   sum(t \in \preset{p}) X(t)·Post(p, t) = sum(t \in \postset{p}) X(t)·Pre(p, t), \forall p \in P (token flow)
 *   
 *   \forall t in T, \forall p in \preset{t} = {p}
 *     x(t) <= m(p)/(s(t)·Pre(p, t))
 * 
 * XXX: Assume non conflict -> otherwise, more constraints should be added (in superclass)
 * 
 * See J. Campos and M. Silva, 
 * "Embedded Product-Form Queueing Networks and the Improvement of Performance Bounds for Petri Net Systems," 
 * Performance Evaluation, vol. 18, iss. 1, pp. 3-19, 1993.
 * http://webdiis.unizar.es/CRPetri/papers/jcampos/93_CS_PE.ps.gz
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2012) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.timed.performance;


import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;

public class UpperAverageMarkingBound extends GeneralBound {
	
	/**
	 * Default constructor, needs PN model to compute its upper performance bound
	 * and the index of the transition for being computed to.
	 * 
	 * @param petriNet
	 * @param idxPlace Index of place to compute its average marking
	 */
	public UpperAverageMarkingBound(PetriNetModel petriNet, int idxPlace)
	{
		super(petriNet, idxPlace);
	}
	
	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		linear.add(1, TAG_PLACE + this.idxTransition); // I leave this name even ii makes a bit of craziness
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MAX);
		
		return objFunc;
	}

	
	protected LPResult processResult(Result result) {
		/* Process LP result */
		LPResult finalResult = null;
		double[] aux = new double[1];
		String[] aux2 = new String[1];
		aux[0] = result.get(TAG_PLACE + idxTransition).doubleValue();
		aux2[0] = this.petriNet.getPlaceID(idxTransition);

		try {
			finalResult = new LPResult(aux, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
		
		return finalResult;
	}

	@Override
	protected LPConstraint[] loadIneqConstraints() {
		// TODO Auto-generated method stub
		return null;
	}

}
