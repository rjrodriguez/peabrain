/**
 * Filename: UpperThroughputBound.java
 * Date: January, 2012 -- first release
 * Defines constraints for computing upper bound of a transition of a PN model
 * 
 *   m = m0 + C·\sigma (reachability)
 *   sum(t \in \preset{p}) X(t)·Post(p, t) = sum(t \in \postset{p}) X(t)·Pre(p, t), \forall p \in P (token flow)
 *   
 *   \forall t in T, \forall p in \preset{t} = {p}
 *     x(t) <= m(p)/(s(t)·Pre(p, t))
 * 
 * XXX: Assume non conflict -> otherwise, more constraints should be added (in superclass)
 * 
 * See J. Campos and M. Silva, 
 * "Embedded Product-Form Queueing Networks and the Improvement of Performance Bounds for Petri Net Systems," 
 * Performance Evaluation, vol. 18, iss. 1, pp. 3-19, 1993.
 * http://webdiis.unizar.es/CRPetri/papers/jcampos/93_CS_PE.ps.gz
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.timed.performance;

import java.util.ArrayList;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;
import pipe.modules.bounds.lpp.solvers.SolverGLPK;
import pipe.modules.bounds.lpp.structural.StructuralMarking;

public class MarkingUpperBound extends PerformanceLPP {
	private LPResult[] resultSI;
	private boolean[] persistence; 
	
	private final String TAG_PLACE = "m";
	private final String TAG_SIGMA = "s";
	private final String TAG_THR = "x";
	private final String TAG_BETA = "b";
	private int idxTransition, idxPlace;
	private double thrValueBound;
	
	/**
	 * Default constructor. It needs the PN model, and the index of transition
	 * to compute the general bound for.
	 * Besides, it invokes to StructuralMarking LP problem (needed for general bound computation)
	 * @param petriNet
	 * @param idxTransition
	 */
	public MarkingUpperBound(PetriNetModel petriNet, int idxTransition, double thrValueBound, int idxPlace)
	{
		super();
		super.setPetriNet(petriNet);
		this.idxTransition = idxTransition;
		this.idxPlace = idxPlace;
		this.thrValueBound = thrValueBound;
		
		/* Compute structural marking bound */
		resultSI = new LPResult[this.nPlaces];
		for(int i = 0; i < resultSI.length; i++)
		{
			StructuralMarking smb = new StructuralMarking(i);
			smb.setSolver(new SolverGLPK());
			smb.setPetriNet(this.petriNet);
			smb.solve();
			/* Get results... */
			resultSI[i] = smb.getResult();
		}
		
		computeStructuralPersistence();
	}
	
	private void computeStructuralPersistence()
	{
		this.persistence = new boolean[nTransitions];
		for(int i = 0; i < nTransitions; i++)
		{
			if(this.petriNet.isImmediate(i))
			{
				persistence[i] = true;
				continue;
			}
			
			// Check possible conflicts ONLY FOR TIMED TRANSITIONS
			ArrayList<Integer> v = this.petriNet.findInColPre(i);
			boolean isPersistent = true;
			
			for(int j = 0; j < v.size() && isPersistent; j++)
			{
				ArrayList<Integer> v2 = this.petriNet.findInRowPre(v.get(j));
				isPersistent &= (v2.size() == 1);
			}
			
			persistence[i] = isPersistent;
		}
		
		return;
	}
	
	@Override
	protected int checkOtherErrors() {
		return 0;
	}

	@Override
	protected LPConstraint[] loadConstraints() {
		/* Get inequality constraints */
		LPConstraint[] ineqConstraints = loadIneqConstraints();
		
		// Compute number of transitions in conflict, and stores it
		ArrayList<Integer> v;
		ArrayList<ArrayList<Integer>>  conflictPlaces = new ArrayList<ArrayList<Integer>>(1);
		int nConstraints = 0;
		for(int i = 0; i < nPlaces; i++)
		{
			v = this.petriNet.findInRowPre(i);
					
			if(v.size() > 1)
			{
				boolean freeConflict = true;
				for(int j = 0; j < v.size() && freeConflict; j++)
				{
					// We need to check now if it's a free-conflict
					ArrayList<Integer> c = this.petriNet.findInColPre(v.get(j));
					freeConflict &= (c.size() == 1);
				}
				if(freeConflict)
				{
					conflictPlaces.add(v);
					nConstraints += v.size() - 1;
				}
			}
			//XXX: We assume only immediate transitions are in conflict...
		}
		
		LPConstraint[] constraints = new LPConstraint[nPlaces*3 + nTransitions*2 + ineqConstraints.length + nConstraints + 3];
	
		/* Create equality constraints */
		/* m = m0 + C·\sigma */ // Reachability
		int aux;
		Linear auxLinear;
		for(int i = 0; i < nPlaces; i++)
		{
			auxLinear = new Linear();
			auxLinear.add(1, TAG_PLACE + i); // m(p_i)
			
			if(i == idxPlace)
				auxLinear.add(-1, TAG_BETA);
			
			if(i == 0)
				auxLinear.add(-1, TAG_BETA + 1);
			if (i == 19)
				auxLinear.add(-1, TAG_BETA + 2);
			
			for(int j = 0; j < nTransitions; j++)
			{
				aux = -this.petriNet.getIncidenceMatrix()[i][j];
				if(aux != 0)
					auxLinear.add(aux, TAG_SIGMA + j);
			}
			// Create constraint m_p - C(p,:)·\sigma = m0(p)
			constraints[i] = 
					new LPConstraint(auxLinear, LPConstants.EQUAL, this.petriNet.getInitialMarkingAtPlace(i));
		}
		/* Token flow */
		/* sum(t \in \preset{p}) X(t)·Post(p, t) >= sum(t \in \postset{p}) X(t)·Pre(p, t), \forall p \in P */
		for(int i = 0; i < nPlaces; i++)
		{
			auxLinear = new Linear();
			v = this.petriNet.findInRowPost(i);
			for(int j = 0; j < v.size(); j++)
			{
				auxLinear.add(this.petriNet.getPost()[i][((Integer)v.get(j)).intValue()], TAG_THR + v.get(j)); // X(t)·Post(p, t)
			}
			v = this.petriNet.findInRowPre(i);
			for(int j = 0; j < v.size(); j++)
			{
				auxLinear.add(-this.petriNet.getPre()[i][((Integer)v.get(j)).intValue()], TAG_THR + v.get(j)); // X(t)·Pre(p, t)
			}
			
			/* Check for the symbol... */
			String symbol = LPConstants.GREATEST_THAN_OR_EQUAL;
			if(resultSI[i].getResult() != null)
				symbol = LPConstants.EQUAL;
			
			constraints[i + nPlaces] = 
					new LPConstraint(auxLinear, symbol, 0);
		}
		
		/* Get inequalities constraints */
		int idx = nPlaces*2;
		for(LPConstraint c : ineqConstraints)
		{
			constraints[idx] = c;
			idx++;
		}
		
		/* Positive values: m, \sigma >= 0, x(t) >= 0 \forall t in T */
		for(int i = 0; i < nPlaces; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_PLACE + i);						
			
			//if(i == idxPlace)
			//{
			//	constraints[i + idx] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, this.thrValueBound);
			//	continue;
			//}
			
			constraints[i + idx] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		idx += nPlaces;
		for(int i = 0; i < nTransitions; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_SIGMA + i);
			
			constraints[i + idx] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		idx += nTransitions;
		for(int i = 0; i < nTransitions; i++)
		{
			Linear linear = new Linear();
			linear.add(1, TAG_THR + i);
			
			if(i == this.idxTransition)
			{
				constraints[i + idx] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, this.thrValueBound);
				continue;
			}
			
			constraints[i + idx] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}
		
		
		idx += nTransitions; 
		// Constraints for routing
		for(int i = 0; i < conflictPlaces.size(); i++)
		{
			v = (ArrayList<Integer>)conflictPlaces.get(i); /* Get conflict transitions */
			int firstElmnt = (int)v.get(0);
					
			for(int j = 1; j < v.size(); j++)
			{
				aux = (int)v.get(j);
				
				auxLinear = new Linear();
				auxLinear.add(this.petriNet.getTransitionWeight(aux), TAG_THR + firstElmnt); // r_k·thr_{t_j}
						
				auxLinear.add(-this.petriNet.getTransitionWeight(firstElmnt), TAG_THR + aux); // r_j·thr_{t_k}
					
				constraints[idx] = 
						new LPConstraint(auxLinear, LPConstants.EQUAL, 0);
									
				idx++;
			}
		}
		
		auxLinear = new Linear();
		auxLinear.add(1, TAG_BETA);
		constraints[idx] = new LPConstraint(auxLinear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		
		auxLinear = new Linear();
		auxLinear.add(1, TAG_BETA + 1);
		constraints[idx + 1] = new LPConstraint(auxLinear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		
		auxLinear = new Linear();
		auxLinear.add(1, TAG_BETA + 2);
		constraints[idx + 2] = new LPConstraint(auxLinear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		
		return constraints;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nPlaces + nTransitions*2 + 3];
		
		for(int i = 0; i < nPlaces; i++)
		{
			vars[i] = new LPVariable(TAG_PLACE + i, Double.class);
		}
		
		for(int i = 0; i < nTransitions; i++)
		{
			vars[nPlaces + i] = new LPVariable(TAG_THR + i, Double.class);
		}
		
		for(int i = 0; i < nTransitions; i++)
		{
			vars[nPlaces + nTransitions + i] = new LPVariable(TAG_SIGMA + i, Double.class);
		}
		
		//vars[nPlaces + nTransitions*2] = new LPVariable(TAG_ALPHA, Double.class); // Should be integer, but low complexity if double
		vars[nPlaces + nTransitions*2] = new LPVariable(TAG_BETA, Integer.class); // Should be integer, but low complexity if double
		vars[nPlaces + nTransitions*2 + 1] = new LPVariable(TAG_BETA + 1, Integer.class); // Should be integer, but low complexity if double
		vars[nPlaces + nTransitions*2 + 2] = new LPVariable(TAG_BETA + 2, Integer.class); // Should be integer, but low complexity if double
		//vars[nPlaces + nTransitions*2 + 1] = new LPVariable(TAG_ALPHA2, Double.class);
		return vars;
	}
	
	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		linear.add(1, TAG_BETA);
		linear.add(1, TAG_BETA + 1);
		linear.add(1, TAG_BETA + 2);
		//for(int i = 0; i < nTransitions; i++)
		//	linear.add(1, TAG_THR + i);
		//linear.add(1, TAG_THR + this.idxTransition);
		//linear.add(1, TAG_PLACE + this.idxPlace);
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MIN);
		
		return objFunc;
	}

	/**
	 * Creates a vector with the corresponding inequality constraints.
	 * @return A vector of constraints
	 */
	protected LPConstraint[] loadIneqConstraints()
	{
		ArrayList<LPConstraint> constraints = new ArrayList<LPConstraint>();

		/* Upper bound inequalities */
		// \forall t  \in T, and \forall p \in \preset(p), x(t) <= m(p)/ (s(t)·Pre(p,t))
		Linear auxLinear;
		ArrayList<Integer> v;
		for(int i = 0; i < nTransitions; i++)
		{
			if(this.petriNet.isImmediate(i))
				continue; // Skip immediate transitions
			
			v = this.petriNet.findInColPre(i);
			
			for(int j = 0; j < v.size(); j++)
			{
				auxLinear = new Linear();
				int idx = (((Integer)v.get(j)).intValue());
				auxLinear.add(1.0,  TAG_THR + i);
				auxLinear.add(-1.0/(1.0/this.petriNet.getTransitionRate(i)*this.petriNet.getPre()[idx][i]), 
									TAG_PLACE + idx);
				
				/* Add constraint */
				constraints.add(new LPConstraint(auxLinear, LPConstants.LOWEST_THAN_OR_EQUAL, 0));
			}
		}
		
		// Lower bound inequalities
		// \forall t in T, \preset{t} = {p}
		/* x(t)·s(t) >= (m(p) - Pre(p, t) + 1)/ Pre(p, t) */
		// \forall t in T, \preset{t} = {p_1, p_2}, where smb(p_1) < smb(p_2) (structural marking bound)
		/* x(t)·s(t)·Pre(p_1, t) >=
		 * 							m(p_1) - Pre(p_1, t) + 1 - smb(p_1)·(1 - (m(p_2)- Pre(p_2, t) + 1)/(smb(p_2) - Pre(p_2, t) + 1)) 
		*/
		double aux2 = 0;
		for(int i = 0; i < nTransitions; i++)
		{
			//if(petriNet.isImmediate(i))
			//{
			//	continue; // Avoid immediate transitions
			//}
			
			// Check persistence
			if(persistence[i])
			{
				auxLinear = new Linear();
				v = this.petriNet.findInColPre(i);
				if(v.size() == 0)
					continue;
					
				int idx = ((Integer)v.get(0)).intValue();
				if(v.size() == 1) // \preset{t} = {p}
				{
					auxLinear.add(1.0/this.petriNet.getTransitionRate(i),  TAG_THR + i);
					//if(resultSI[idx].getResult() != null) // is bounded
					/*{
						int k = (int) (resultSI[idx].getResult()[0]/this.petriNet.getPre()[idx][i]);
						auxLinear.add(-k/(resultSI[idx].getResult()[0] - k*this.petriNet.getPre()[idx][i] + 1),  TAG_PLACE + idx);
						aux2 = k*(-k*this.petriNet.getPre()[idx][i] + 1)/(resultSI[idx].getResult()[0] - k*this.petriNet.getPre()[idx][i] + 1);
					}
					else*/
					{
						auxLinear.add(-1.0/this.petriNet.getPre()[idx][i],  TAG_PLACE + idx);
						aux2 = -1 + 1.0/this.petriNet.getPre()[idx][i];
					}
				}
				else // \preset{t} = {p_1, p_2}
					{
						double 	smb1 = resultSI[idx].getResult()[0], 
								smb2 = resultSI[(int)v.get(1)].getResult()[0];
						int idx2 = (int)v.get(1);
						if(smb1 > smb2)
						{
							aux2 = smb1;
							smb1 = smb2;
							smb2 = aux2;
							idx = (int)v.get(1);
							idx2 = (int)v.get(0);
						}
						
						auxLinear.add(this.petriNet.getPre()[idx][i]*1.0/this.petriNet.getTransitionRate(i),  TAG_THR + i);
						auxLinear.add(-1, TAG_PLACE + idx);
						auxLinear.add(-smb1/(smb2 - this.petriNet.getPre()[idx2][i] + 1),  TAG_PLACE + idx2);
						aux2 = -this.petriNet.getPre()[idx][i] + 1 - smb1;
						aux2 += smb1*(-this.petriNet.getPre()[idx2][i] + 1)/(smb2 - this.petriNet.getPre()[idx2][i] + 1);
					}
					constraints.add( 
							new LPConstraint(auxLinear, LPConstants.GREATEST_THAN_OR_EQUAL, aux2));
			}
		}
		
		LPConstraint[] constraintsLP = new LPConstraint[constraints.size()];
		int i = 0;
		for(LPConstraint lpc : constraints)
			constraintsLP[i++] = lpc;
				
		return constraintsLP;
	}
	

	@Override
	protected LPResult processResult(Result result) {
		/* Process LP result */
		LPResult finalResult = null;
		double[] aux = new double[4];
		String[] aux2 = new String[4];
		aux[0] = result.get(TAG_THR + idxTransition).doubleValue();
		aux2[0] = this.petriNet.getTransitionID(idxTransition);
		
		aux[1] = result.get(TAG_BETA).doubleValue();
		aux2[1] = this.petriNet.getPlaceID(idxPlace);
		
		aux[2] = result.get(TAG_BETA + 1).doubleValue();
		aux2[2] = this.petriNet.getPlaceID(0);
		
		aux[3] = result.get(TAG_BETA + 2).doubleValue();
		aux2[3] = this.petriNet.getPlaceID(19);
		try {
			finalResult = new LPResult(aux, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
		
		return finalResult;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		return null;
	}

}
