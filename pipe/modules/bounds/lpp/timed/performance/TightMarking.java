/**
 * Filename: TightMarking.java
 * Date: January, 2012 -- first release
 * 
 * Computes the tight marking (tm) the Petri net model
 *  See the next paper for tight marking definition:
 *  R. J. Rodríguez and J. Júlvez, 
 *    "Accurate Performance Estimation for Stochastic Marked Graphs by Bottleneck Regrowing"
 *     in Proceedings of the 7th European Performance Engineering Workshop (EPEW), 2010, pp. 175-190.
 *     (http://webdiis.unizar.es/~ricardo/files/papers/RJ-EPEW-10.pdf)
 *  
 *  max sum(\sigma)
 *     \delta(p·)·\Theta <= tm(p), for every p \in P
 *     tm = m0 + C·\sigma
 *     \sigma(t_p) = k
 *     
 *     where \Theta is the upper throughput bound, t_p is any transition belonging to the critical cycle
 *     and \sigma is the firing count vector.
 *     
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp.timed.performance;


import java.util.ArrayList;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Result;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPConstants;
import pipe.modules.bounds.lpp.LPConstraint;
import pipe.modules.bounds.lpp.LPObjectiveFunc;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.LPVarBound;
import pipe.modules.bounds.lpp.LPVariable;

public class TightMarking extends PerformanceLPP {
	private final String TAG_PLACE = "tm";
	private final String TAG_SIGMA = "s";
	
	private final int idxTransition;
	private final double theta;
	
	/**
	 * Default constructor. It needs the throughput of the PN, and the index
	 * of a transition belonging to the critical cycle.
	 * @param theta Throughput of the system (critical cycle)
	 * @param idxTransition Index of a transition which belongs to the critical cycle
	 */
	public TightMarking(double theta, int idxTransition)
	{
		this.theta = theta;
		this.idxTransition =idxTransition;
	}
	
	@Override
	protected int checkOtherErrors() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected LPConstraint[] loadConstraints() {
		LPConstraint[] constraints = new LPConstraint[nPlaces*2 + 1];
		
		/* Create inequality constraints */
		// \delta(p·)·\Theta <= tm(p), for every p \in P
		for(int i = 0; i < nPlaces; i++)
		{
			/* Get p· */
			ArrayList<Integer> v = this.petriNet.findInRowPre(i);
			double aux = 0; // It will be an immediate transition, by PN constraints!

			if(v.size() == 1)
			{
				aux = this.petriNet.getTransitionRate(((Integer)v.get(0)).intValue());
				aux = (1.0/aux)*theta; // \delta(p·)·\Theta
			}
			
			Linear linear = new Linear();
			linear.add(1, TAG_PLACE + i);
			
			constraints[i] = new LPConstraint(linear, LPConstants.GREATEST_THAN_OR_EQUAL, aux);
		}
		
		/* Create equality constraints */
		/* tm = m0 + C·\sigma */
		int aux;
		Linear auxLinear;
		for(int i = 0; i < nPlaces; i++)
		{
			auxLinear = new Linear();
			auxLinear.add(1, TAG_PLACE + i); // tm(p_i)
			for(int j = 0; j < nTransitions; j++)
			{
				aux = -this.petriNet.getIncidenceMatrix()[i][j];
				if(aux != 0)
					auxLinear.add(aux, TAG_SIGMA + j);
			}
			// Create constraint tm_p - C(p,:)·\sigma = m0(p)
			constraints[(i + nPlaces)] = 
					new LPConstraint(auxLinear, LPConstants.EQUAL, this.petriNet.getInitialMarkingAtPlace(i));
		}
		
		/* Constraint: \sigma(t_p) = k */
		auxLinear = new Linear();
		auxLinear.add(1, TAG_SIGMA + this.idxTransition);
		constraints[nPlaces*2] = 
				new LPConstraint(auxLinear, LPConstants.EQUAL, 100); // Any k constant
		
		/*for(int i = 0; i < nTransitions; i++)
		{
			auxLinear = new Linear();
			auxLinear.add(1, TAG_SIGMA + i); // \sigma_i
			constraints[(nPlaces*2 + 1) + i] = new LPConstraint(auxLinear, LPConstants.GREATEST_THAN_OR_EQUAL, 0);
		}*/
		
		return constraints;
	}

	@Override
	protected LPObjectiveFunc loadObjectiveFunction() {
		LPObjectiveFunc objFunc;
		
		Linear linear = new Linear();
		for(int i = 0; i < nTransitions; i++)
			linear.add(1,  TAG_SIGMA + i);
		
		/* Set objective function*/
		objFunc = new LPObjectiveFunc(linear, OptType.MAX);
		
		return objFunc;
	}

	@Override
	protected LPVariable[] loadVariablesType() {
		LPVariable[] vars = new LPVariable[nPlaces + nTransitions];
		
		for(int i = 0; i < nPlaces; i++)
		{
			vars[i] = new LPVariable(TAG_PLACE + i, Double.class);
		}
		
		for(int i = 0; i < nTransitions; i++)
		{
			vars[nPlaces + i] = new LPVariable(TAG_SIGMA + i, Double.class);
		}
		
		return vars;
	}

	@Override
	protected LPResult processResult(Result result) {
		/* Process LP result */
		LPResult finalResult = null;
		
		double[] auxD = new double[nPlaces];
		String[] aux2 = new String[nPlaces];
		
		for(int i = 0; i < nPlaces; i++)
		{
			aux2[i] = this.petriNet.getPlaceID(i);
			auxD[i] = result.get(TAG_PLACE + i).doubleValue();			
		}
		
		try {
			finalResult = new LPResult(auxD, aux2);
		} catch (ResultException e) {
			e.printStackTrace();
		}
		
		return finalResult;
	}

	@Override
	protected LPVarBound[] getVarUpperBounds() {
		/* No bounds needed */
		return null;
	}

	@Override
	protected LPVarBound[] getVarLowerBounds() {
		return null;
	}

}
