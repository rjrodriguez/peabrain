/**
 * Filename: MyRender.java
 * Date: January, 2014 -- first release
 * 
 * Draw row and columns in the SolverSelector Window.
 *
 * @author (C) Iván Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */

package pipe.modules.bounds.lpp.solvers.solverSelector;

import javax.swing.JTable;
import java.awt.Component;
import java.awt.Font;
import javax.swing.table.DefaultTableCellRenderer;

public class MyRender extends DefaultTableCellRenderer
{
	private static final long serialVersionUID = 1L;
	private SolverSelectorModel model = null;
	
	public MyRender(SolverSelectorModel m){
		this.model = m;
	}
   
   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, 
	  boolean hasFocus, int row, int column)
   {
	   super.getTableCellRendererComponent (table, value, isSelected, hasFocus, row, column);
	   boolean 	focusable 	= false,
	   			enabled 	= false;

	   if(column == 0 && model.getSolverAvailable(value))
	   {
		   enabled = true;
		   focusable = true;
		   if(value.equals(model.getDefaultSolver()))
			   this.setFont(this.getFont().deriveFont(Font.BOLD));    
	   }
	   //this.setBackground(rowColour);
	   
	   this.setEnabled(enabled);
	   this.setFocusable(focusable);
    		  
      return this;
   }
}