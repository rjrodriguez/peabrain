/**
 * Filename: MyTableModel.java
 * Date: January, 2014 -- first release
 * 
 * Set Solver Selector Table struct.
 *
 * @author (C) Iván Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */

package pipe.modules.bounds.lpp.solvers.solverSelector;

import javax.swing.table.AbstractTableModel;


public class MyTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = -8749161257384582250L;
	private String [] columnNames = {"Solver", "Availability", "Path"};
	private String[][] data;
    private SolverSelectorModel model = null;
 
    public MyTableModel (SolverSelectorModel m){
    	this.model = m;
    	data = model.getSolverList();
    }
    
    public int getRowCount() {
    	if(data == null)
    		data = model.getSolverList();
        return data.length;
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return data[rowIndex][columnIndex];
    }
    public String getColumnName(int col) {
            return columnNames[col];
    }

    public boolean isCellEditable(int row, int col) {
                return false;
    }

    public void setValueAt(int row,Object[] value) {

    }
}
