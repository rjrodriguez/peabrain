/**
 * Filename: SolverSelectorGUI.java
 * Date: January, 2014 -- first release
 * 
 * Implements the GUI solver selector of MVC.
 *
 * @author (C) Iván Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */

package pipe.modules.bounds.lpp.solvers.solverSelector;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SolverSelectorGUI extends JFrame {

	private static final long serialVersionUID = 5002155173058757210L;
	private JPanel contentPane = null;
	private SolverSelectorModel model = new SolverSelectorModel();
	private JTable table = null;
	MyTableModel myModel = new MyTableModel(model);
	private SolverSelectorController controller = new SolverSelectorController(model,myModel);


	/** 
	 * Creates the frame.
	 */
	
	public SolverSelectorGUI(){

		setTitle("Solver Selector");
		setBounds(100, 100, 600, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblCurrentlySelectedSolver = new JLabel("Currently selected ILP solver:");
		lblCurrentlySelectedSolver.setFont(new Font("Dialog", Font.BOLD, 16));
		
		JScrollPane scrollPane = new JScrollPane(table);

		final JLabel DefaultSolver = new JLabel(model.getDefaultSolver());
		
		JButton btnCancel = new JButton("Cancel");
		//Button "Cancel" action
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				}
		});
		
		//Button "Accept" action
		JButton btnAccept = new JButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.applyButton(table.getSelectedRow());
				if (myModel.getValueAt(table.getSelectedRow(), 1).toString().equals("Available")){
					DefaultSolver.setText(model.getStringID(table.getSelectedRow()));
					}
				dispose();
			}
		});
		
		JButton btnApply = new JButton("Apply");
		btnApply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.applyButton(table.getSelectedRow());
				if (myModel.getValueAt(table.getSelectedRow(), 1).toString().equals("Available")){
					DefaultSolver.setText(model.getStringID(table.getSelectedRow()));
					}
				table.repaint();
			}
		});
		
		JLabel lblSolversInThe = new JLabel("Solvers in the system:");
		
		DefaultSolver.setFont(new Font("Dialog", Font.BOLD, 16));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(155, Short.MAX_VALUE)
					.addComponent(btnApply)
					.addGap(18)
					.addComponent(btnAccept)
					.addGap(18)
					.addComponent(btnCancel)
					.addContainerGap())
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGap(28)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 548, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(lblSolversInThe, GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
							.addGap(190))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(lblCurrentlySelectedSolver, GroupLayout.PREFERRED_SIZE, 282, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(DefaultSolver)
							.addContainerGap(76, Short.MAX_VALUE))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCurrentlySelectedSolver, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
						.addComponent(DefaultSolver))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblSolversInThe)
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCancel)
						.addComponent(btnAccept)
						.addComponent(btnApply))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
		
		table = new JTable(myModel)
		{
			private static final long serialVersionUID = 7364716402524295283L;

			public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
			{
				Component c = super.prepareRenderer(renderer, row, column);

				//  Alternate row color

				if (!isRowSelected(row))
				{
					c.setBackground(getBackground());
					int modelRow = convertRowIndexToModel(row);
					String type = (String)getModel().getValueAt(modelRow, 1);
					if ("Available".equals(type))
						c.setBackground(Color.GREEN);
					else
						c.setBackground(Color.RED);
				}

				return c;
			}
		};
		table.putClientProperty(myModel, false);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setColumnSelectionAllowed(false);
		table.setCellSelectionEnabled(false);
		table.setRowSelectionAllowed(true);
		table.setSelectionForeground(Color.black);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		table.setDefaultRenderer(Object.class, new MyRender(model));

		scrollPane.setViewportView(table);
		setVisible(true);
	}
	
	public SolverSelectorModel getModel(){
		return model;
	}
	
	public MyTableModel getMyTableModel(){
		return myModel;
	} 
}
