/**
 * Filename: LPVariable.java
 * Date: January, 2012 -- first release
 * Defines a variable of the LP problem and its type (integer or double, depending on the LP problem). 
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */

package pipe.modules.bounds.lpp;

public class LPVariable {
	private String variable;
	private Class<?> varType;
	
	/**
	 * Creates a LPVariable object. It needs the variable, as String, and its type, as Class
	 * @param variable to be defined
	 * @param varType Type of the variable
	 */
	public LPVariable(String variable, Class<?> varType)
	{
		this.setVariable(variable);
		this.setVarType(varType);
	}
	
	/**
	 * Returns the type of this variable
	 * @return Class type
	 */
	public Class<?> getVarType() {
		return varType;
	}
	private void setVarType(Class<?> varType) {
		this.varType = varType;
	}
	/**
	 * Gets the identification of the variable
	 * @return Variable identification
	 */
	public String getVariable() {
		return variable;
	}
	private void setVariable(String variable) {
		this.variable = variable;
	}
}
