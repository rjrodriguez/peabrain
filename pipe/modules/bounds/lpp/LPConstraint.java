/**
 * Filename: LPConstraint.java
 * Date: January, 2012 -- first release
 * Describes a Linear Programming constraint
 * It has three attributes: Linear, sign and value
 * Example: 120x + 210y <= 1000
 * it should be an object 
 * 		linear = new Linear();
 * 		linear.add(120, "x"); linear.add(210, "y");
 * 		LPConstraint example;
 * 		example.setEquation(linear, LPConstants.LOWEST_THAN_OR_EQUAL, 1000);
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.lpp;

import net.sf.javailp.Linear;

public class LPConstraint {
	private Linear equation;
	private double value;
	private String sign;
	
	/**
	 * Constructor for LPConstraint. It needs the equation (left hand side, LHS),
	 * the sign (<=, = or >=) and the value (right hand side, RHS)
	 * @param equation LHS of the inequation
	 * @param sign Type of the constraint, <=, = or >=. Better use LPConstants definition
	 * @param value RHS of the inequation
	 */
	public LPConstraint(Linear equation, String sign, double value)
	{
		this.setEquation(equation);
		this.setSign(sign);
		this.setValue(value);
	}
	
	/**
	 * Gets the left-hand side element of the constraint
	 * @return A Linear equation
	 */
	public Linear getEquation() {
		return equation;
	}
	private void setEquation(Linear equation) {
		this.equation = equation;
	}
	
	/**
	 * Gets the right-hand side element of the constraint
	 * @return A double value
	 */
	public double getValue() {
		return value;
	}
	private void setValue(double value) {
		this.value = value;
	}
	
	/**
	 * Gets the sign of the constraint. It can be <=, = or >=
	 * @return Sign (as String)
	 */
	public String getSign() {
		return sign;
	}

	private void setSign(String sign) {
		this.sign = sign;
	}
	
}
