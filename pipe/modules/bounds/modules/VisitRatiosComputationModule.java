/**
 * Filename: VisitRatiosComputationModule.java
 * Date: January, 2012 -- first release
 * 
 * PIPE module, invokes the VisitRatiosComputationGUI for computing 
 * visit ratio normalised for a given transition for the current PN view 
 * See VisitRatio.java and VisitRatiosStrategy.java for more details
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.modules;

import pipe.gui.ApplicationSettings;
import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.gui.VisitRatiosComputationGUI;
import pipe.modules.bounds.strategies.VisitRatiosStrategy;
import pipe.modules.interfaces.IModule;
import pipe.views.PetriNetView;

public class VisitRatiosComputationModule implements IModule {
	private static String MODULE_NAME = "Visit Ratios Computation";
	
	@Override
	public String getName() {
		return MODULE_NAME;
	}

	@Override
	public void start() {
		PetriNetView pnmlData = ApplicationSettings.getApplicationView().getCurrentPetriNetView();
		PetriNetModel pnModel = new PetriNetModel(pnmlData);

		VisitRatiosStrategy vrs = new VisitRatiosStrategy(pnModel);
		
		@SuppressWarnings("unused")
		VisitRatiosComputationGUI vra = new VisitRatiosComputationGUI(vrs);
	}

}
