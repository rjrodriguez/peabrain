/**
 * Filename: ErrorMessage.java
 * Date: January, 2012 -- first release
 * Defines an static method to invoke a MessageDialog with an error messag
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.errors;

import javax.swing.JOptionPane;

public class ErrorMessage {
	private static String 	NO_PN_DEFINED = "No Petri net defined.\nPlease check your code",
							NO_SOLVER_DEFINED = "No LP solver defined.\nPlease check your code.",
							PN_DEFINED_EMPTY = "The Petri net defined is empty.\nAre you sure you have drawn something? :)",
							PN_TYPE_NO_MG = "The Petri net is not a Marked Graph (i.e., |p·| = |·p| = 1).\nPlease checks its structure and relaunch bound computation.",
							NO_BUDGET_GIVEN = "Please check assigned budget -- some error arise while conversion to double format.",
							NO_UNIQUESOLUTION_VISITRATIO = "Visit ratio does not have a unique solution. This feature cannot be computed.",
							NO_INITIAL_MARKING_VALID = "Check the initial marking. No simulation step is possible with such an initial marking.";
	
	/**
	 * Invokes a message dialog with an error message
	 * @param error Error constant
	 * @return the given error as parameter
	 */
	public static int showErrorMessage(int error)
	{
		String errorMsg = "";
		
		switch(error)
		{
			case ErrorConstants.NO_PNMODEL_DEFINED:
				errorMsg = ErrorMessage.NO_PN_DEFINED;
				break;
			case ErrorConstants.NO_SOLVER_DEFINED:
				errorMsg = ErrorMessage.NO_SOLVER_DEFINED;
				break;
			case ErrorConstants.PN_DEFINED_EMPTY:
				errorMsg = ErrorMessage.PN_DEFINED_EMPTY;
				break;
			case ErrorConstants.PN_TYPE_NO_MG:
				errorMsg = ErrorMessage.PN_TYPE_NO_MG;
				break;
			case ErrorConstants.NO_BUDGET_GIVEN:
				errorMsg = ErrorMessage.NO_BUDGET_GIVEN;
				break;
			case ErrorConstants.NO_UNIQUESOLUTION_VISITRATIO:
				errorMsg = ErrorMessage.NO_UNIQUESOLUTION_VISITRATIO;
				break;
			case ErrorConstants.NO_INITIAL_MARKING_VALID:
				errorMsg = ErrorMessage.NO_INITIAL_MARKING_VALID;
				break;
		}
		
		/* Show message dialog with the proper error */
		JOptionPane.showMessageDialog(null, errorMsg, "ERROR - check details", JOptionPane.ERROR_MESSAGE);
		
		/* Return the own error code */
		return error;
	}
}
