/**
 * Filename: ResourceOptimisationStrategy.java
 * Date: January, 2012 -- first release
 * 
 * Implements the resource optimisation strategy. It tries to optimise performance of a PN model
 * by adjusting the number of resources for a given budget and a given cost per resource
 * 
 * See the next paper for more details:
 * TODO: Add a reference to the paper
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.strategies;

import java.util.ArrayList;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ErrorMessage;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.WellFormedLPP;
import pipe.modules.bounds.lpp.solvers.SolverGLPK;
import pipe.modules.bounds.lpp.timed.performance.SlowestPSemiflow;
import pipe.modules.bounds.lpp.timed.resource.NextConstraintResource;

public class ResourceOptimisationStrategy extends Strategy {
	private String processIdle;
	private double[] vCost;
	private int[] idxResources;
	private double budget;
	private double restBudget;
	private boolean canFollow;
	private boolean processIdlePlaceConstraining;
	
	/* Result */
	private double[] resourceIncrement;
	
	/**
	 * Default constructor.
	 * 
	 * @param pnModel Petri net model to work with
	 */
	public ResourceOptimisationStrategy(PetriNetModel pnModel)
	{
		super(pnModel);
	}
	
	/**
	 * Sets the vector of resources
	 * @param idxResources Vector with resource place indexes
	 */
	public void setParametersIdxResources(int[] idxResources)
	{
		this.idxResources = idxResources;
		this.resourceIncrement = new double[idxResources.length];
	}
	
	/**
	 * Sets the process-idle place of the system
	 * @param processIdle Identification label of process-idle place
	 */
	public void setParametersProcessIdle(String processIdle)
	{
		this.processIdle = processIdle;
	}
	
	/**
	 * Sets the vector cost, which assigns a cost to each resource
	 * @param vCost Vector which assigns a cost per resource
	 */
	public void setParametersCostVector(double[] vCost)
	{
		this.vCost = vCost;
	}
	
	/**
	 * Sets budget parameter
	 * @param budget Total budget to be assigned
	 */
	public void setParametersBudget(double budget)
	{
		this.budget = budget;
	}

	/* XXX: We should check parameters are properly set... */
	
	@Override
	protected void computeStrategy()
	{
	
		LPResult auxResult = null;
		
		/* Compute initial bottleneck */
		SlowestPSemiflow sps = new SlowestPSemiflow();
		sps.setPetriNet(this.pnModel);
		/* Set solver */
		sps.setSolver(new SolverGLPK());
		
		/* Check if it's possible to compute */
		int error = sps.checkOtherErrors();
		if(error != 0)
		{ /* It can't be computed, then report it with a message error */
			ErrorMessage.showErrorMessage(error);
			return;
		}
		else
		{
			// Solve it
			sps.solve();
			// Get result
			LPResult slowestPSemiflow = sps.getResult();
			//System.out.println(sps);
			
			int k = 0;
			double cost = 0;
			double[] auxIncrement = null;
			
			
			int pSize = this.pnModel.getPlacesSize();
			double[][] yValues = new double[idxResources.length][pSize];
			for(int i = 0; i < pSize; i++)
				yValues[0][i] = slowestPSemiflow.getResult()[i + 1];		
			int idxProcessIdle = this.pnModel.getPlaceIndex(processIdle);
			
			while(cost < budget && k != idxResources.length && (yValues[k][idxProcessIdle] <= WellFormedLPP.getTolerance()))
			{
				k++;
				double auxCost = cost;
				resourceIncrement =  auxIncrement;
				int[] a_j = new int[k];
				/* Create variable for the next LPP */
				double[][] yAux = new double[k][pSize];
				for(int i = 0; i < k; i++)
				{
					for(int j = 0; j < pSize; j++)
						yAux[i][j] = yValues[i][j];
					
					/* Create vector of resources for next LPP */
					/* Set resource indexes */
					for(int r = 0; r < idxResources.length; r++)
						if(yAux[i][idxResources[r]] > 0)
							a_j[i] = idxResources[r];
				}

				

				auxResult = null;
				/* Compute next constraint resource */
				try {
					NextConstraintResource ncr = new NextConstraintResource(pnModel, a_j, yAux);
					ncr.setSolver(new SolverGLPK());
					
					/* Solve it */
					ncr.solve();
					auxResult = ncr.getResult();
					
					if(k != idxResources.length)
						for(int j = 0; j < pSize; j++)
							yValues[k][j] = auxResult.getResult()[k + j];
					
					//System.out.println(ncr);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(auxResult == null)
				{
					/* Error... */
					System.err.println("No result in the last LPP (k=" + k + ")");
					return;
				}
				
				/* Compute costs */
				cost = 0;
				auxIncrement = new double[idxResources.length];
				for(int i = 0; i < k; i++)
				{
					int j, idxR = this.pnModel.getPlaceIndex(auxResult.getVariables()[i]);
					for(j = 0; j < idxResources.length; j++)
						if(idxResources[j] == idxR)
							break;
					
					auxIncrement[j] = (int) Math.ceil(auxResult.getResult()[i]);					
					cost += auxIncrement[j]*vCost[j];
				}
				restBudget = budget - auxCost;
			}
			
			if(k == 0) // Initial p-semiflow already contains the process idle place, exiting...
			{
				processIdlePlaceConstraining = true;
				resourceIncrement = null;
				restBudget = budget;
				return;
			}
			
			/* End of strategy */
			if(k == idxResources.length && cost <= budget && auxResult.getResult()[k + idxProcessIdle] != 0)
			{
				resourceIncrement =  auxIncrement;
				restBudget = budget - cost;
			}
			else if (k < idxResources.length && cost <= budget)
			{
				// There remain money to be spent, and we can increment more...
				resourceIncrement =  auxIncrement;
				canFollow = (auxResult != null && auxResult.getResult()[k + idxProcessIdle] <= WellFormedLPP.getTolerance());
				restBudget = budget - cost;
			}else
				k--;
			
			/* Set results... */
			result = new LPResult[1];
			String[] auxS = new String[k];
			double[] auxR = new double[k];
			
			for(int i = 0; i < k; i++)
			{
				auxS[i] = this.pnModel.getPlaceID(idxResources[i]);
				auxR[i] = resourceIncrement[i];
			}
			
			try 
			{
				if(resourceIncrement != null)
					result[0] = new LPResult(auxR, auxS);
			} catch (ResultException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public ArrayList<String> getResultsHeader() {
		ArrayList<String> l = new ArrayList<String>();
		
		l.add("Resource place");
		l.add("Increment");
		
		return l;
	}
	
	/**
	 * Overrides Strategy.printResult -> needed for adding slowest p-semiflow information
	 */
	public String printResult()
	{
		StringBuilder s = new StringBuilder();
		
		/* Create extra-results for unassigned budget */
		s.append("<b>Remaining budget</b>: $" + restBudget + "<br/>");
		
		if(resourceIncrement == null)
		{
			if(processIdlePlaceConstraining)
				s.append("The number of resources in process-idle place is initially constraining the system.");
			else
				s.append("Insufficient initial budget.");
			
			return s.toString();
		}
		
		s.append("More increments CAN");
		if(!canFollow)
			s.append("NOT");
		s.append(" be done with such a configuration.<br/>");

		/* Get other results */
		s.append(super.printResult());
		
		/* Get visit ratio, and puts it */
		s.append("<br/><br/><b>Computations done taking visit ratio normalised for transition " + this.pnModel.getNormalisedVisitRatioTransitionID() + 
				".</b><br/><b>v</b> = {");
		for(int i = 0; i < this.pnModel.getTransitionsSize(); i++)
			s.append(this.pnModel.getVisitRatioAtTransition(i) + ", ");
		s.replace(s.length() - 2, s.length(), "}<br/>");
		
		return s.toString();
	}

}
