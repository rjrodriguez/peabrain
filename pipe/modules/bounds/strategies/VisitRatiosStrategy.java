/**
 * Filename: VisitRatiosStrategy.java
 * Date: January, 2012 -- first release
 * 
 * Implements the visit ratio computation for a given Petri net, normalised for a given transition
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.strategies;

import java.util.ArrayList;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ResultException;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.structural.VisitRatio;

public class VisitRatiosStrategy extends Strategy {
	private int idxNormalisedTransition;
	private boolean uniqueSolution;
	
	/**
	 * Default constructor
	 * 
	 * @param pnModel Petri net model to work with
	 */
	public VisitRatiosStrategy(PetriNetModel pnModel)
	{
		super(pnModel);
	}
	
	/**
	 * Sets the idx of the normalised transition
	 * 
	 * @param normalisedTransitionID Identification label of the normalised transition
	 * @throws Exception if the identification label of the transition is not found for the given Petri net model
	 */
	public void setNormalisedTransition(String normalisedTransitionID) throws Exception
	{
		this.idxNormalisedTransition = this.pnModel.getTransitionIndex(normalisedTransitionID);
		if(this.idxNormalisedTransition == -1)
			throw new Exception("Transition " + normalisedTransitionID + " not found. Please, set values properly.");
	}
	
	@Override
	public ArrayList<String> getResultsHeader() {
		ArrayList<String> l = new ArrayList<String>();
		
		l.add("Transition");
		l.add("Visit ratio");
		
		return l;
	}
	
	@Override
	protected void computeStrategy()
	{
		/* Compute visit ratio */
		this.pnModel.recomputeVisitRatio(idxNormalisedTransition);
		
		VisitRatio vr = new VisitRatio(pnModel, idxNormalisedTransition);
		uniqueSolution = vr.hasUniqueSolution();
		
		result = new LPResult[1];
		
		String[] aux = new String[this.pnModel.getTransitionsSize()];
		double[] aux2 = new double[aux.length];
		for(int i = 0; i < aux.length; i++)
		{
			aux[i] = this.pnModel.getTransitionID(i);
			aux2[i] = this.pnModel.getVisitRatioAtTransition(i);
		}
		
		try {
			result[0] = new LPResult(aux2, aux);
		} catch (ResultException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Overrides Strategy.printResult -> needed for adding slowest p-semiflow information
	 */
	public String printResult()
	{
		StringBuilder s = new StringBuilder();
		/* Get visit ratio, and puts it */
		s.append("<b>Visit ratios normalised for transition " + 
					this.pnModel.getNormalisedVisitRatioTransitionID() + ".</b><br/>");

		if(this.pnModel.isErrorVisitRatiosComputation())
		{
			s.append("<b>Warning</b>: the visit ratios computation CANNOT be computed.");
			return s.toString();
		}
		
		/* Check and reports if the solution is unique or not */
		s.append("The visit ratios <b>solution IS ");
		if(!uniqueSolution)
			s.append("NOT ");
		s.append("unique</b>.");
		
		return s.toString() + super.printResult();
	}
}
