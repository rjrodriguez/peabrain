/**
 * Filename: LinearBoundStrategy.java
 * Date: January, 2012 -- first release
 * 
 * Implements the linear bound computation strategy.
 * Gets the needed parameters, and invoke to LinearBound.java, which
 * implements the LP problem for computing upper/lower bounds
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */
package pipe.modules.bounds.strategies;

import java.util.ArrayList;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.solvers.SolverGLPK;
import pipe.modules.bounds.lpp.timed.performance.MarkingUpperBound;

public class MarkingUpperBoundStrategy extends Strategy
{
	private int idxPlace = -1, idxTransition = -1;
	private double thrBoundValue = -1;
	
	public MarkingUpperBoundStrategy(PetriNetModel pnModel) {
		super(pnModel);
	}
	
	public void setPlace(int idxPlace)
	{
		this.idxPlace = idxPlace;
	}
	
	public void setTransition(int idxTransition)
	{
		this.idxTransition = idxTransition;
	}
	
	public void setThrBoundValue(double thrBoundValue)
	{
		this.thrBoundValue = thrBoundValue;
	}
	
	@Override
	protected void computeStrategy()
	{
		if(this.idxPlace == -1 || this.idxTransition == -1 || this.thrBoundValue == -1)
			return;
			
		result = new LPResult[1];
		
		MarkingUpperBound mub = new MarkingUpperBound(this.pnModel, this.idxTransition, this.thrBoundValue, this.idxPlace);
		mub.setSolver(new SolverGLPK());
		mub.solve();
			
		/* Set results */
		result[0] = mub.getResult();
	}
	
	@Override
	public ArrayList<String> getResultsHeader() {
		ArrayList<String> l = new ArrayList<String>();
		
		l.add("Place");
		l.add("N. tokens to add");
		
		return l;
	}

}
