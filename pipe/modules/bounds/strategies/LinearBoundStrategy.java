/**
 * Filename: LinearBoundStrategy.java
 * Date: January, 2012 -- first release
 * 
 * Implements the linear bound computation strategy.
 * Gets the needed parameters, and invoke to LinearBound.java, which
 * implements the LP problem for computing upper/lower bounds
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */
package pipe.modules.bounds.strategies;

import java.util.ArrayList;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ErrorMessage;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.WellFormedLPP;
import pipe.modules.bounds.lpp.solvers.SolverGLPK;
import pipe.modules.bounds.lpp.timed.performance.LowerAverageMarkingBound;
import pipe.modules.bounds.lpp.timed.performance.PerformanceLPP;
import pipe.modules.bounds.lpp.timed.performance.UpperAverageMarkingBound;
import pipe.modules.bounds.lpp.timed.performance.LowerThroughputBound;
import pipe.modules.bounds.lpp.timed.performance.SlowestPSemiflow;
import pipe.modules.bounds.lpp.timed.performance.UpperThroughputBound;


public class LinearBoundStrategy extends Strategy
{
	private boolean[] selectedComputations = new boolean[5];
	private LPResult[] intermmediateResults = new LPResult[5];
	// 0 -> slowestPSemiflow, 1 -> lowerThr, 2 -> upperThr, 3 -> lowerMarking, 4 -> upperMarking
	private int lowerT, upperT, upperP, lowerP;
	private int nItem;

	public LinearBoundStrategy(PetriNetModel pnModel) {
		super(pnModel);
	}
	
	/**
	 * Sets parameters needed for computing upper average marking bound of a place
	 * @param computeAverageMarking Boolean to determine if we have to compute average marking
	 * @param upperP Index of place to be computed
	 */
	public void setParametersForUpperAverageMarkingComputation(boolean computeUpperAverageMarking, int upperP)
	{
		this.selectedComputations[4] = computeUpperAverageMarking;
		this.upperP = upperP;
	}
	
	/**
	 * Sets parameters needed for computing lower average marking bound of a place
	 * @param computeAverageMarking Boolean to determine if we have to compute average marking
	 * @param avgM Index of place to be computed
	 */
	public void setParametersForLowerAverageMarkingComputation(boolean computeLowerAverageMarking, int lowerP)
	{
		this.selectedComputations[3] = computeLowerAverageMarking;
		this.lowerP = lowerP;
	}
	
	

	/**
	 * Sets parameters needed for computing lower bound
	 * @param computeLowerBound Boolean to determine if we have to compute lower bound
	 * @param lowerT Index of transition to compute its lower bound
	 */
	public void setParametersForLowerBoundComputation(boolean computeLowerBound, int lowerT)
	{
		this.selectedComputations[1] = computeLowerBound;
		this.lowerT = lowerT;
	}
	
	/**
	 * Sets parameters needed for computing upper bound
	 * @param computeUpperBound Boolean to determine if we have to compute upper bound
	 * @param upperT Index of transition to compute its upper bound
	 */
	public void setParametersForUpperBoundComputation(boolean computeUpperBound, int upperT)
	{
		this.selectedComputations[2] = computeUpperBound;
		this.upperT = upperT;
	}
	
	/**
	 * Sets parameters needed for computing slowest upper bound
	 * @param computeSlowest Boolean to determine if we have to compute, 
	 * 							besides, the slowest P-semiflow of the PN model
	 */
	public void setParametersForSlowestBoundComputation(boolean computeSlowest)
	{
		this.selectedComputations[0] = computeSlowest;
	}
	
	
	@Override
	protected void computeStrategy()
	{
		if(this.selectedComputations[0])
		{
			/* Compute slowest p-semiflow */
			SlowestPSemiflow sps = new SlowestPSemiflow();
			sps.setPetriNet(this.pnModel);
			/* Set solver */
			sps.setSolver(new SolverGLPK());
			
			/* Check if it's possible to compute */
			int error = sps.checkOtherErrors();
			if(error != 0) /* It can't be computed, then report it with a message error */
				ErrorMessage.showErrorMessage(error);
			else
			{
				// Solve it
				sps.solve();
				// Get result
				intermmediateResults[0] = sps.getResult();
				//System.out.println(sps);
			}
		}
		
		/* Check what else we should compute... */
		for(int i = 1; i < this.selectedComputations.length; i++)
		{
			if(!this.selectedComputations[i])
				continue;
			
			PerformanceLPP lpp = null; 
			
			switch(i)
			{
			case 1:
				lpp = new LowerThroughputBound(this.pnModel, this.lowerT);
				break;
			case 2:
				lpp = new UpperThroughputBound(this.pnModel, this.upperT);
				break;
			case 3:
				lpp = new LowerAverageMarkingBound(this.pnModel, this.lowerP);
				break;
			case 4:
				lpp = new UpperAverageMarkingBound(this.pnModel, this.upperP);
				break;
			default:
				return;	
			}
			
			lpp.setSolver(new SolverGLPK());
			lpp.solve();
			
			intermmediateResults[i] = lpp.getResult();
		}
	}
	
	@Override
	public ArrayList<String> getResultsHeader() {
		ArrayList<String> l = new ArrayList<String>();
		
		switch(nItem)
		{
		case 1:
			l.add("Transition");
			l.add("Lower throughput bound");
			break;
		case 2:
			l.add("Transition");
			l.add("Upper throughput bound");
			break;
		case 3:
			l.add("Place");
			l.add("Lower avg. marking bound");
			break;
		case 4:
			l.add("Place");
			l.add("Upper avg. marking bound");
			break;
		default:
			return null;	
		}
		
		return l;
	}
	
	/**
	 * Overrides Strategy.printResult -> needed for adding slowest p-semiflow information
	 */
	public String printResult()
	{
		StringBuilder s = new StringBuilder();
		if(selectedComputations[0] && this.intermmediateResults[0] != null)
		{
			/* Create extra-results for slowest p-semiflow */
			s.append("<b>Slowest p-semiflow</b>: {");
			for(int i = 1; i < this.intermmediateResults[0].getVariables().length; i++)
				if(this.intermmediateResults[0].getResult()[i] > WellFormedLPP.getTolerance())
					s.append(this.intermmediateResults[0].getVariables()[i] + ", ");
			s.replace(s.length() - 2, s.length(), "}<br/>");
			s.append("<b>Throughput</b>: " + (1.0/this.intermmediateResults[0].getResult()[0])); // Thr is in the first element...
			s.append("<br/><br/>");
		}
		
		for(nItem = 1; nItem < this.intermmediateResults.length; nItem++)
		{
			if(this.intermmediateResults[nItem] != null && selectedComputations[nItem])
			{
				LPResult[] auxResult = new LPResult[1];
				auxResult[0] = this.intermmediateResults[nItem];
				s.append(printResult(auxResult));
			}
			
		}
		
		/* Get visit ratio, and puts it */
		if(selectedComputations[0] && this.intermmediateResults[0] != null)
		{
			s.append("<br/><br/><b>Computations done taking visit ratio normalised for transition " + this.pnModel.getNormalisedVisitRatioTransitionID() + 
				".</b><br/><b>v</b> = {");
			for(int i = 0; i < this.pnModel.getTransitionsSize(); i++)
				s.append(this.pnModel.getVisitRatioAtTransition(i) + ", ");
			s.replace(s.length() - 2, s.length(), "}<br/>");
		}
		
		return s.toString();
	}

}
