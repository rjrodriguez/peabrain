/**
 * Filename: PerformanceEstimationStrategy.java
 * Date: January, 2012 -- first release
 * 
 * Implements the performance estimation strategy.
 * First, it computes the slowest p-semiflow of the system.
 * After that, it iterates adding the most likely components to be constraining
 * the current system to the critical cycle, and recomputes the slowest p-semiflow.
 * For each one, the throughput is computed by simulation.
 * 
 * See the next paper for more details:
 * TODO: Add a reference to the paper
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.strategies;

import java.util.ArrayList;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.errors.ErrorMessage;
import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.WellFormedLPP;
import pipe.modules.bounds.lpp.solvers.SolverGLPK;
import pipe.modules.bounds.lpp.structural.HValue;
import pipe.modules.bounds.lpp.timed.performance.NextSlowestPSemiflow;
import pipe.modules.bounds.lpp.timed.performance.SlowestPSemiflow;
import pipe.modules.gspnsimulator.ExactSimulator;
import pipe.modules.gspnsimulator.SimulationMethod;
import pipe.modules.gspnsimulator.Simulator;

public class PerformanceEstimationStrategy extends Strategy
{
	private double epsilon;
	private ArrayList<PerformanceResult> results;
	private long totalPEtime = 0;
	
	/**
	 * Default constructor.
	 * of precision to be achieved
	 * @param pnModel Petri net model to work with
	 */
	public PerformanceEstimationStrategy(PetriNetModel pnModel)
	{
		super(pnModel);
		results = new ArrayList<PerformanceResult>(1);
	}
	
	/**
	 * Sets the epsilon parameter for computation stuff
	 * @param epsilon Degree of precision to be achieved
	 */
	public void setParameterEpsilon(double epsilon)
	{
		this.epsilon = epsilon;
	}

	@Override
	public ArrayList<String> getResultsHeader() {
		ArrayList<String> l = new ArrayList<String>();
		
		l.add("Regrowing step");
		l.add("Components");
		l.add("Throughput");
		l.add("Error accuracy");
		l.add("Sim. time");
		
		return l;
	}
	
	class PerformanceResult
	{
		ArrayList<Integer> components;
		double thr;
		double simTime;
		double errAcc;
		
		PerformanceResult(ArrayList<Integer> components, 
							double thr, double simTime, double errAcc)
		{
			this.components = components;
			this.thr = thr;
			this.simTime = simTime;
			this.errAcc = errAcc;
		}
	}
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void computeStrategy()
	{
	    int[][] originalPre = new int[this.pnModel.getPlacesSize()][this.pnModel.getTransitionsSize()],
	    		refPre = this.pnModel.getPre(),
	    		originalPost = new int[this.pnModel.getPlacesSize()][this.pnModel.getTransitionsSize()],
	    		refPost = this.pnModel.getPost();
	    /* Get original Pre, Post */
	    for(int i = 0; i < originalPre.length; i++)
	    	for(int j = 0; j < originalPre[0].length; j++)
	    	{
	    		originalPre[i][j] = refPre[i][j];
	    		originalPost[i][j] = refPost[i][j];
	    	}
					
		/* Compute initial bottleneck */
		SlowestPSemiflow sps = new SlowestPSemiflow();
		sps.setPetriNet(this.pnModel);
		/* Set solver */
		sps.setSolver(new SolverGLPK());
		
		/* Check if it's possible to compute */
		int error = sps.checkOtherErrors();
		if(error != 0)
		{ /* It can't be computed, then report it with a message error */
			ErrorMessage.showErrorMessage(error);
			return;
		}
		// Solve it
		sps.solve();
		// Get result
		LPResult slowestPSemiflow = sps.getResult();
		
		/* Compute h */
		HValue h = new HValue();
		h.setPetriNet(pnModel);
		h.setSolver(new SolverGLPK());
		
		long start = System.currentTimeMillis();
		h.solve();
		long duration = System.currentTimeMillis() - start;
		
		double hValue = h.getResult().getResult()[0];		
		
		ArrayList<Integer> Q = this.getComponentsFromResult(slowestPSemiflow);
		PerformanceResult pr = new PerformanceResult(Q, 1.0/slowestPSemiflow.getResult()[0], duration, 0);
		results.add(pr); // First result: slowest p-semiflow

		
		double theta = 1.0/slowestPSemiflow.getResult()[0], // First component is theta
				theta2 = theta*2;
		
		LPResult auxResult = null;
		while((theta2 - theta)/theta2 >= epsilon && Q.size() != this.pnModel.getPlacesSize())
		{
			//System.out.println((theta2 - theta)/theta2);
			/* Compute neighbors of components in Q */
			ArrayList neighbors = new ArrayList(1);
		    for(int i = 0; i < Q.size(); i++)
		    {
		    	ArrayList<Integer> auxT = this.pnModel.findInRowPre((int)Q.get(i));
		    	 
		    	for(int j = 0; j < auxT.size(); j++)
		    	{
		    		ArrayList<Integer> auxP = this.pnModel.findInColPre((int)auxT.get(j));
		    		 
		    		for(int k = 0; k < auxP.size(); k++)
		    		{
		    			if(Q.contains(auxP.get(k)))
		    				continue;
		    			// Add new neighbour
		    			neighbors.add(auxP.get(k));
		    		}
		    	}
		    }
		    
		    /* Get the next p semiflow */
		    NextSlowestPSemiflow nsps = new NextSlowestPSemiflow(neighbors, Q, hValue);
		    nsps.setPetriNet(pnModel);
		    nsps.setSolver(new SolverGLPK());
		    nsps.solve();
		    auxResult = nsps.getResult();
		    		    
		    /* Get new thr */
		    theta2 = theta;
		    
		    /* Simulate... */
		    /* Create new components for Q... */
		    Q = this.getComponentsFromResult(auxResult);
		    ArrayList<Integer> transPSemiflow = getTransitionsOfPSemiflow(Q);
		    /* Remove such components from PRE, POST matrix for performing simulation step */
		    for(int ii = 0; ii < transPSemiflow.size(); ii++)
		    {
		    	// Get places in PRE, POST of transition ii
		    	int idxTrans = transPSemiflow.get(ii);
		    	ArrayList<Integer> pPRE_ii = this.pnModel.findInColPre(idxTrans);
		    	for(int j = 0; j < pPRE_ii.size(); j++)
		    	{
		    		int idxPlace = pPRE_ii.get(j);
		    		if(Q.contains(idxPlace))
		    			continue;
		    		// Whether the place is not contained in the current p-semiflow, erase it
		    		refPre[idxPlace][idxTrans] = 0;
		    	}
		    	
		    	ArrayList<Integer> pPOST_ii = this.pnModel.findInColPost(idxTrans);
		    	for(int j = 0; j < pPOST_ii.size(); j++)
		    	{
		    		int idxPlace = pPOST_ii.get(j);
		    		if(Q.contains(idxPlace))
		    			continue;
		    		// Whether the place is not contained in the current p-semiflow, erase it
		    		refPost[idxPlace][idxTrans] = 0;
		    	}
		    }
		    
		    Simulator s = new ExactSimulator(pnModel);
		    s.simulate(0.95, 0.05); // Conf: 95%, error: 5%
		    // T_0 by default
		    theta = s.getEstimatedThr()[0];
		    totalPEtime += s.getSimulationTime();
		    
		    results.add(pr = new PerformanceResult(Q, theta, s.getSimulationTime(), s.getSimulationError()[0]));
		    
		    /* Restore PRE, POST */
		    this.pnModel.setPre(originalPre);
		    this.pnModel.setPost(originalPost);
		}
		
	}
	
	private ArrayList<Integer> getTransitionsOfPSemiflow(ArrayList<Integer> pSemiflow)
	{
		ArrayList<Integer> Q = new ArrayList<Integer>(1);
		for(int i = 0; i < pSemiflow.size(); i++)
		{
			ArrayList<Integer> transPOST = this.pnModel.findInRowPost(pSemiflow.get(i));
			for(int j = 0; j < transPOST.size(); j++)
			{
				int idxTrans = transPOST.get(j);
				if(!Q.contains(idxTrans))
					Q.add(idxTrans);
			}
		}
		
		return Q;
	}
	
	private ArrayList<Integer> getComponentsFromResult(LPResult r)
	{
		/* Get components of slowest p semiflow */
		ArrayList<Integer> Q = new ArrayList<Integer>(1);
		String[] aux = r.getVariables();
		for(int i = 1; i < aux.length; i++) // Starts in 1 (0 is the thr)
		{
			if(r.getResult()[i] > WellFormedLPP.getTolerance())
				Q.add(new Integer(this.pnModel.getPlaceIndex(aux[i])));
		}
		
		return Q;
	}
	
	/**
	 * Overrides Strategy.printResult
	 */
	public String printResult()
	{
		ArrayList<String> array = this.getResultsHeader();
		PerformanceResult pr;
		
		for(int i = 0; i < results.size(); i++)
		{
			pr = results.get(i);
			ArrayList<Integer> v = (ArrayList<Integer>)pr.components;
			
			array.add(i + "");
			
			StringBuilder s = new StringBuilder();
			
			s.append("{");
			for(int j = 0; j < v.size(); j++)
			{
				s.append(this.pnModel.getPlaceID((int)v.get(j)) + ", ");
			}
			s.replace(s.length() - 2, s.length(), "}");
			array.add(s.toString());
			
			// Add thr
			array.add(String.format("%.6f", pr.thr));
			// Add simulation error
			array.add(String.format(Character.toChars(177)[0] + "%.4f", pr.errAcc));
			// Add simulation time
			array.add(String.format("%.4f", pr.simTime/1000));
		}
		
		String aux = String.format("<br/><b>Total elapsed time:</b> %.4f seconds.",  this.totalPEtime/1000.0);
		return super.printResult(array.toArray(), 5) + aux;
	}
}
