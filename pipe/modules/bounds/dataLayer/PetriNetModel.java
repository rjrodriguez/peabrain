/**
 * Filename: PetriNetModel.java
 * Date: January, 2012 -- first release
 * Defines a Petri net at its lowest level (matricial form)
 *  
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.bounds.dataLayer;

import java.util.ArrayList;
import java.util.LinkedList;

import pipe.modules.bounds.lpp.LPResult;
import pipe.modules.bounds.lpp.solvers.SolverGLPK;
import pipe.modules.bounds.lpp.structural.VisitRatio;
import pipe.views.ArcView;
import pipe.views.ConnectableView;
import pipe.views.InhibitorArcView;
import pipe.views.MarkingView;
import pipe.views.PetriNetView;
import pipe.views.PlaceView;
import pipe.views.TransitionView;

public class PetriNetModel {
	private int Pre[][],
				Post[][],
				C[][],
				PreInhibitor[][];
	private int m0[], priority[];
	private double delay[], weight[], visitRatio[];
	private int idxNormalised = -1;
	private boolean[] infiniteSemantics;
	private double sumOfDelays;
	private boolean errorVisitRatios;
	private boolean uniqueSolution;
	
	private String[] placesID, transitionsID;
	
	public PetriNetModel()
	{
		/* Empty constructor... */
	}
	
	/**
	 * Creates a Petri net object in matricial form
	 * 
	 * @param pnmlData Petri net in PetriNetView format (PIPE data model)
	 */
	public PetriNetModel(PetriNetView pnmlData)
	{
		//System.out.println(System.getProperty("java.class.path"));
		/* Create Petri net raw data */
		ArrayList<PlaceView> places = pnmlData.getPlacesArrayList();
		ArrayList<TransitionView> transitions = pnmlData.getTransitionsArrayList();
		ArrayList<ArcView> arcs = pnmlData.getArcsArrayList();
		ArrayList<InhibitorArcView> inhibitorArcs = pnmlData.getInhibitorsArrayList();
		
		
		/* Init data */
		Pre = new int[places.size()][transitions.size()];
		Post = new int[places.size()][transitions.size()];
		C = new int[places.size()][transitions.size()];
		PreInhibitor = new int[places.size()][transitions.size()];
		m0 = new int[places.size()];
		delay = new double[transitions.size()];
		weight = new double[transitions.size()];
		priority = new int[transitions.size()];
		infiniteSemantics = new boolean[transitions.size()];
		
		placesID = new String[places.size()];
		transitionsID = new String[transitions.size()];
		
		/* Create initial marking vector */
		int idx = 0;
		for(PlaceView place : places)
		{
			LinkedList<MarkingView> currentMarking = place.getCurrentMarkingObject();
			if(currentMarking.size() > 0)
				/* XXX: How many initial markings are?? */
				m0[idx] = currentMarking.getFirst().getCurrentMarking();
			
			placesID[idx] = place.getId();
			idx++;
		}
		/* Create delay and ratio vectors */
		idx = 0;
		for(TransitionView transition : transitions)
		{
			weight[idx] = -1; // Set ratio -1 for timed transitions
			/* Check if is timed for getting the rate delay or the weight */
			if(transition.isTimed())
				delay[idx] = 1/transition.getRate();
			else
			{
				weight[idx] = transition.getRate();
				priority[idx] = transition.getPriority();
			}
			
			transitionsID[idx] = transition.getId();
			infiniteSemantics[idx] = transition.isInfiniteServer();

			sumOfDelays += delay[idx];
			idx++;
		}
		
		for(ArcView arc : arcs)
		{
			/* Get arc source and check its type */
			int idxMatrix[][], idxPlace, idxTrans, sign;
			ConnectableView place, 
							transition;
			
			if(arc.getSource() instanceof PlaceView)
			{
				idxMatrix = Pre;
				place = arc.getSource();
				transition = arc.getTarget();
				sign = -1;
			}
			else
			{
				idxMatrix = Post;
				place = arc.getTarget();
				transition = arc.getSource();
				sign = 1;
			}
			
			/* Complete the transition*/
			idxPlace = places.indexOf(place);
			idxTrans = transitions.indexOf(transition);
			idxMatrix[idxPlace][idxTrans] = arc.getWeight().getFirst().getCurrentMarking();
			this.C[idxPlace][idxTrans] += sign*arc.getWeight().getFirst().getCurrentMarking();
		}
		
		for(InhibitorArcView arc : inhibitorArcs)
		{
			ConnectableView place = arc.getSource();
			ConnectableView transition = arc.getTarget();
			int idxPlace = places.indexOf(place);
			int idxTrans = transitions.indexOf(transition);
			if(arc.getWeight().size() != 0)
				this.PreInhibitor[idxPlace][idxTrans] = arc.getWeight().getFirst().getCurrentMarking();
			else
				this.PreInhibitor[idxPlace][idxTrans] = 1; // Prevent error on non-modified PIPE -> incomplete InhibitorArcView class
			// It doesn't allow to put some weight on it!!
			//XXX: Assume 1 by default
		}
	}
	
    /**
     * Returns the Pre matrix which defines the current Petri net object
     * @return Pre matrix
     */
    public int[][] getPre()
    {
        return Pre;
    }
    
    /**
     * Returns the Post matrix  which defines the current Petri net object
     * @return Post matrix
     */
    public int[][] getPost()
    {
        return Post;
    }
    
    /**
     * Returns the incidence matrix of the Petri net, i.e., C = Post - Pre
     * (it's pre-computed at loading, for saving running time when invoking)
     * @return C matrix
     */
    public int[][] getIncidenceMatrix()
    {
        return C;
    }
    
    public int[] getM0()
    {
        return m0;
    }
    
    public double[] getDelay()
    {
        return delay;
    }
    
    public int getTransitionsSize()
    {
        if(Pre.length != 0)
        	return Pre[0].length;
        else
        	return 0;
    }
    
    
    public int getPlacesSize()
    {
        return Pre.length;
    }
    
    /**
     * Find the positive elements in Pre(row,:), i.e.,
     * returns L where L={ t | Pre(row, t) > 0, \forall t in T},
     * being T the set of transitions of the Petri net
     * 
     * @param row to look for
     * @return L
     */
	public ArrayList<Integer> findInRowPre(int row)
    {
    	return findInRow(Pre, row);
    }
    
    /**
     * Find the positive elements in Post(row,:), i.e.,
     * returns L where L={ t | Post(row, t) > 0, \forall t in T},
     * being T the set of transitions of the Petri net
     * 
     * @param row to look for
     * @return L
     */
	public ArrayList<Integer>  findInRowPost(int row)
    {
    	return findInRow(Post, row);
    }
    
    /**
     * Find the positive elements in Pre(:, col), i.e.,
     * returns L where L={ p | Pre(p, col) > 0, \forall p in P},
     * being P the set of places of the Petri net
     * 
     * @param col column to look for
     * @return L
     */
	public ArrayList<Integer>  findInColPre(int col)
    {
    	return findInCol(Pre, col);
    }
	
	//TODO Javadoc
	public ArrayList<Integer>  findInColPreInhibitor(int col)
    {
    	return findInCol(PreInhibitor, col);
    }
    
    
    /**
     * Find the positive elements in Post(:, col), i.e.,
     * returns L where L={ p | Post(p, col) > 0, \forall p in P},
     * being P the set of places of the Petri net
     * 
     * @param col column to look for
     * @return L
     */
	public ArrayList<Integer>  findInColPost(int col)
    {
    	return findInCol(Post, col);
    }
    
    /**
     * Gets positive values of m(row, :)
     * @param m A matrix
     * @param row The index to look for
     * @return Returns a vector with all positive values in matrix m
     */
	private ArrayList<Integer> findInRow(int[][] m, int row)
    {
        if(m == null)
            return null;
               
        ArrayList<Integer> positiveValues = new ArrayList<Integer>(2);
        int colLength = m[0].length;
        
        for(int i = 0; i < colLength; i++)
        {
           if(m[row][i] > 0)
           {
               positiveValues.add(new Integer(i));
           }
        }
        
        return positiveValues;
    }
    
    /**
     * Gets positive values of m(:, col)
     * @param m A matrix
     * @param col The index to look for
     * @return Returns a vector with all positive values in matrix m
     */
	private ArrayList<Integer> findInCol(int[][] m, int col)
    {
        if(m == null)
            return null;
               
        ArrayList<Integer> positiveValues = new ArrayList<Integer>(2);
        int rowLength = m.length;
        
        for(int i = 0; i < rowLength; i++)
        {
           if(m[i][col] > 0)
           {
               positiveValues.add(new Integer(i));
           }
        }
        
        return positiveValues;
    }
    
    /**
     * Gets positive values in the vector, i.e., 
     * return L= {i | v(i) > 0}
     * 
     * @param v A vector
     * @return Returns a vector with all positive values in vector v
     */
	private ArrayList<Integer> find(int[] v)
    {
        if(v == null)
            return null;
               
        ArrayList<Integer> positiveValues = new ArrayList<Integer>(2);
        int colLength = v.length;
        
        for(int i = 0; i < colLength; i++)
        {
           if(v[i] > 0)
           {
               positiveValues.add(new Integer(i));
           }
        }
        
        return positiveValues;
    }
    
    /**
     * Gets the indexes of places p with marking(p) > 0, i.e.,
     * returns L = {p | marking(p) > 0, p \in P}
     * where P is the set of places in the Petri net.
     * Throws an exception if length of marking vector does not match
     * the number of places.
     * 
     * @param marking Marking to get the places marked at
     * @return L
     * @throws Exception if length of marking vector (parameter) does not match
     * the number of places.
     */
	public ArrayList<Integer>  getPlacesMarkedAt(int[] marking) throws Exception
    {
    	if(marking.length > this.getPlacesSize())
    		throw new Exception("Error: expected marking does not match the number of places of the PN.");
    	
    	return find(marking);
    }
    
    /**
     * Gets the indexes of places p with m0(p) > 0, i.e.,
     * returns L = {p | m0(p) > 0, p \in P}
     * where P is the set of places in the Petri net.
     * 
     * @return L
     */
    public ArrayList<Integer>  getPlacesMarkedAtInitialMarking()
    {
    	try {
			return getPlacesMarkedAt(this.m0);
		} catch (Exception e) {
			return null; // Never will fall here -> for sure |m0| should be equal to |P|!
		}
    }
    
    /**
     * Returns the transition rate of the transition given as parameter
     * @param idxTransition index of transition
     * @return Transition rate (1/delay(idxTransition))
     */
    public double getTransitionRate(int idxTransition)
    {
        return 1/delay[idxTransition];
    }
    
    /**
     * Method to print the object as a string.
     * @return A string representing the current Petri net object
     */
    public String toString()
    {
    	/* Print PRE matrix */
    	StringBuilder s = new StringBuilder();
    	
    	s.append("Pre matrix:" + "[" + this.getPlacesSize() + ";" + this.getTransitionsSize() + "]" + "\n");
    	if(Pre.length != 0)
    	{
	    	for(int i = 0; i < Pre.length; i++)
	    	{
	    		s.append("[");
	    		for(int j = 0; j < Pre[0].length; j++)
	    		{
	    			s.append(Pre[i][j] + "  ");
	    		}
	    		s.delete(s.length() - 2, s.length());
	    		s.append("]\n");
	    	}
	    	s.append("\n");
    	}
    	
    	/* Print POST matrix */
    	s.append("Post matrix:" + "[" + this.getPlacesSize() + ";" + this.getTransitionsSize() + "]" + "\n");
    	if(Post.length != 0)
    	{
	    	for(int i = 0; i < Post.length; i++)
	    	{
	    		s.append("[");
	    		for(int j = 0; j < Post[0].length; j++)
	    		{
	    			s.append(Post[i][j] + "  ");
	    		}
	    		s.delete(s.length() - 2, s.length());
	    		s.append("]\n");
	    	}
	    	s.append("\n");
    	}
    	
    	/* Print C matrix */
    	s.append("Incidence matrix:" + "[" + this.getPlacesSize() + ";" + this.getTransitionsSize() + "]" + "\n");
    	if(C.length != 0)
    	{
	    	for(int i = 0; i < C.length; i++)
	    	{
	    		s.append("[");
	    		for(int j = 0; j < C[0].length; j++)
	    		{
	    			s.append(C[i][j] + "  ");
	    		}
	    		s.delete(s.length() - 2, s.length());
	    		s.append("]\n");
	    	}
	    	s.append("\n");
    	}
    	
    	/* Print m0 vector */
    	s.append("Initial marking:" + "[" + this.getPlacesSize() + "]" + "\n");
    	if(m0.length != 0)
    	{
			s.append("[");
	    	for(int i = 0; i < m0.length; i++)
	    		s.append(m0[i] + "  ");
			s.delete(s.length() - 2, s.length());
			s.append("]\n\n");
    	}
    	
		/* Print delay vector */
    	s.append("Delay:" + "[" + this.getTransitionsSize() + "]" + "\n");
    	if(delay.length != 0)
    	{
			s.append("[");
	    	for(int i = 0; i < delay.length; i++)
	    		s.append(delay[i] + "  ");
			s.delete(s.length() - 2, s.length());
			s.append("]\n\n");
	    }
    	return s.toString();
    }
    
    /**
     * Returns the identification label of place idxPlace
     * @param idxPlace index of place to seek
     * @return Identification label
     */
    public String getPlaceID(int idxPlace)
    {
    	return placesID[idxPlace];
    }
    
    /**
     * Returns the index of place identified by idxPlace
     * @param idPlace identification label of place to seek
     * @return Index place
     */
    public int getPlaceIndex(String idPlace)
    {
    	for(int i = 0; i < placesID.length; i++)
    		if(placesID[i].equals(idPlace))
    			return i;
    	return -1; // -1 = not found
    }
    
    /**
     * Returns the identification label of transition idxTransition
     * @param idxTransition index of transition to seek
     * @return Identification label
     */
    public String getTransitionID(int idxTransition)
    {
    	return transitionsID[idxTransition];
    }
    
    /**
     * Returns the index of transition identified by idxTransition
     * @param idTransition identification label of transition to seek
     * @return Index place
     */
    public int getTransitionIndex(String idTransition)
    {
    	for(int i = 0; i < transitionsID.length; i++)
    		if(transitionsID[i].equals(idTransition))
    			return i;
    	return -1; // -1 = not found
    }
    
    
    /**
     * Checks if the current PN is an Ordinary Marked Graph, 
     * i.e., \forall p \in P, |·p| = |p·| = 1
     * @return A boolean indicating if the current PB object is an Ordinary Marked Graph
     */
    @SuppressWarnings("rawtypes")
	public boolean isMarkedGraph()
    {
    	int len = this.getPlacesSize();
    	ArrayList aux, aux2;
    	for(int i = 0; i < len; i++)
    	{
    		aux = this.findInRow(Pre, i);
    		aux2 = this.findInRow(Post, i);
    		if(!(aux.size() == aux2.size() &&
    				aux.size() == 1 &&
    				Pre[i][((Integer) aux.get(0)).intValue()] == 1 && Post[i][((Integer) aux2.get(0)).intValue()] == 1)) // Ordinary MG
    			return false;
    	}
    	return true;
    }
    
    /**
     * Checks if the current PN is a Weighted Marked Graph, 
     * i.e., \forall p \in P, |·p| = |p·| = 1
     * @return A boolean indicating if the current PB object is an Weighted Marked Graph (weight arcs >= 1)
     */
	@SuppressWarnings("rawtypes")
    public boolean isWeightedMarkedGraph()
    {
    	int len = this.getPlacesSize();
		ArrayList aux, aux2;
    	for(int i = 0; i < len; i++)
    	{
    		aux = this.findInRow(Pre, i);
    		aux2 = this.findInRow(Post, i);
    		if(!(aux.size() == aux2.size() &&
    				aux.size() == 1)) //  MG
    			return false;
    	}
    	return true;
    }
    
    /**
     * Returns if the place idxPlace has an initial marking, i.e.,
     * m0(idxPlace) > 0
     * @param idxPlace Index of place to look for
     * @return m0(idxPlace) > 0 
     */
    public boolean isInitialMarked(int idxPlace)
    {
    	return (this.m0[idxPlace] > 0);
    }
    
    /**
     * Returns the initial marking at place idxPlace
     * @param idxPlace Index of place to look for
     * @return m0(idxPlace)
     */
    public int getInitialMarkingAtPlace(int idxPlace)
    {
    	return this.m0[idxPlace];
    }
    
    /**
     * Returns the weight of the transition idxTransition
     * Warning: timed transitions don't have weight
     * @param idxTransition Index of transition to look for
     * @return Weight of transition idxTransition
     */
    public double getTransitionWeight(int idxTransition)
    {
    	return this.weight[idxTransition];
    }
    
    /**
     * Gets the visit ratio at transition idxTransition
     * Visit ratio are computed the first time that it's invoked,
     * and it's computed normalised for transition t_0
     * @param idxTransition Index of transition
     * @return Visit ratio of transition idxTransition
     * normalised for transition t_0
     */
    public double getVisitRatioAtTransition(int idxTransition)
    {
    	if(visitRatio == null)
    	{
    		visitRatio = new double[this.getTransitionsSize()];
    		computeVisitRatio(0); // XXX: Compute visit ratio normalised for t_0
    	}
    	
    	return visitRatio[idxTransition];
    }
    
    /**
     * Computes visit ratio normalised for transition idxNormalisedTransition
     * @param idxNormalisedTransition Index of normalised transition
     */
    private void computeVisitRatio(int idxNormalisedTransition)
    {
    	/* Get visit ratios */
    	this.idxNormalised = idxNormalisedTransition;
		VisitRatio vr = new VisitRatio(this, idxNormalised); //XXX: Normalised for 0 by default
		vr.setSolver(new SolverGLPK());
		vr.solve();
		LPResult v = vr.getResult();
		
		if((errorVisitRatios = (v == null))) // Error, not can be computed...
			return;
		
		for(int i = 0; i < this.getTransitionsSize(); i++)
		{
			visitRatio[i] = v.getResult()[i];
		}
		this.uniqueSolution = vr.hasUniqueSolution();
    }
    
    public boolean isErrorVisitRatiosComputation()
    {
    	return this.errorVisitRatios;
    }
    
    /**
     * Recomputes visit ratio changing the normalised transition to idxNormalisedTransition
     * @param idxNormalisedTransition Index of new normalised transition
     */
    public void recomputeVisitRatio(int idxNormalisedTransition)
    {
    	if(visitRatio == null)
    	{
    		visitRatio = new double[this.getTransitionsSize()];
    	}
    	if(idxNormalisedTransition != this.idxNormalised)
    		this.computeVisitRatio(idxNormalisedTransition);
    }
    
    public boolean hasVisitRatioUniqueSolution()
    {
    	if(visitRatio == null)
    	{
    		visitRatio = new double[this.getTransitionsSize()];
    	}
    	return uniqueSolution;
    }
    
    /**
     * Returns the identification label of transition 
     * used for normalising the visit ratio calculus
     * @return Identification label
     */
    
    public String getNormalisedVisitRatioTransitionID()
    {
    	if(idxNormalised == -1)
    		this.recomputeVisitRatio(0);
    	return this.getTransitionID(idxNormalised);
    }
    
    /**
     * Returns true if the transition idxTransition is immediate,
     * false otherwise
     * @param idxTransition Index of transition to look for
     * @return A boolean indicating if the transition idxTransition is immediate or not
     */
    public boolean isImmediate(int idxTransition)
    {
    	return (delay[idxTransition] == 0);
    }
    
    /**
     * Returns the enabling degree of a transition for a given marking
     * @param marking Current marking of the net to be considered
     * @param idxTransition Index of transition
     * @return The enabling degree of the transition identified by idxTransition at marking
     */
    public int getEnablingDegree(int[] marking, int idxTransition)
    {
    	double ed = Double.MAX_VALUE;
    	
		ArrayList<Integer> places = findInColPre(idxTransition);
		for(int j = 0; j < places.size(); j++)
		{
			int idxPlace = (int)places.get(j);
			double aux = marking[idxPlace]/(double)Pre[idxPlace][idxTransition];
			if(aux < ed)
				ed = aux;
		}
    			
    	return (int)Math.ceil(ed);
    }
    
    /**
     * Returns true if the transition idxTransition has infinite semantics
     * false otherwise
     * @param idxTrans Index of transition to look for
     * @return A boolean indicating if the transition idxTransition has infinite semantics or not
     */
    public boolean isInfiniteSemantics(int idxTrans)
    {
    	return infiniteSemantics[idxTrans];
    }

    /**
     * Returns the sum of delays of the current Petri net
     * (pre-computed at the beginning), i.e., 
     * returns s = sum_{i=1..|T|} (\delta(i)), 
     * where T is the set of transitions of the Petri net
     * @return The sum of delays
     */
	public double getSumOfDelays() {
		return sumOfDelays;
	}

	/**
	 * Set m0 as current initial marking
	 * @param m0 Initial marking to set
	 * @throws Exception if the intiial marking dimension does not match the number of places in the Petri net
	 */
	public void setM0(int[] m0) throws Exception {
		if(m0.length != this.getPlacesSize())
			throw new Exception("Error while assigning a new initial marking. No matching dimensions with number of places.");
		
		this.m0 = m0;
	}
	
	//TODO Javadoc
	public int getPriority(int idxTransition)
	{
		if(idxTransition < 0 || idxTransition >= this.priority.length)
			return -1;
		
		return this.priority[idxTransition];
	}
	
	public int[][] getPreInhibitor()
	{
		return this.PreInhibitor;
	}

	/**
	 * Sets the Pre-condition matrix for the PN
	 * WARNING: No consistency is checked!
	 * @param Pre New Pre matrix
	 */
	public void setPre(int[][] Pre) {
		/* TODO: Check consistency */
		this.Pre = Pre;
	}
	
	/**
	 * Sets the Post-condition matrix for the PN
	 * WARNING: No consistency is checked!
	 * @param Post New Post matrix
	 */
	public void setPost(int[][] Post) {
		/* TODO: Check consistency */
		this.Post = Post;
	}

	public void recomputeIncidenceMatrix()
	{
		C = new int[Pre.length][Pre[0].length];
		
		for(int i = 0; i < Pre.length; i++)
			for(int j = 0; j < Pre[0].length; j++)
				C[i][j] = Post[i][j] - Pre[i][j];
	}
	
	/**
	 * Sets the delay vector for the PN
	 * WARNING: No consistency is checked!
	 * @param delay New delay vector
	 */
	public void setDelay(double[] delay) {
		/* TODO: Check consistency */
		this.delay = delay;
	}
	
	/**
	 * Sets the transition IDs vector for the PN
	 * WARNING: No consistency is checked!
	 * @param transitionsID New transition IDs vector
	 */
	public void setTransitionsID(String[] transitionsID) {
		/* TODO: Check consistency */
		this.transitionsID = transitionsID;
	}
	
	/**
	 * Sets the places IDs vector for the PN
	 * WARNING: No consistency is checked!
	 * @param placesID New transition IDs vector
	 */
	public void setPlacesID(String[] placesID) {
		/* TODO: Check consistency */
		this.placesID = placesID;
	}

}
