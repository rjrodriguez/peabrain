/**
 * Filename: SlowestPSemiflow.java
 * Date: January, 2014 -- first release
 * 
 * Sets values of params for SlowestPSemiflow module.
 * 
 * See SlowestPSemiflowStrategy.java
 * @author (C) Ivan Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.cli.submodules;

import pipe.modules.bounds.cli.MyParamValue;
import pipe.modules.bounds.cli.PeabraiNCLI;
import pipe.modules.bounds.strategies.*;

public class SlowestPSemiflow extends PeabraiNCLI {
		
		public SlowestPSemiflow(){
			super();
		}		
		
		public void buildParamValueVector(){
			myParamValue = new MyParamValue[3+1];
			myParamValue[3] = new MyParamValue("-s","slowest","slowest",false,null,false,1);
		}
	
		@Override
		protected void createStrategy() {
			strategy = new LinearBoundStrategy(pnModel);
			((LinearBoundStrategy) strategy).setParametersForSlowestBoundComputation(true);
			//XXX Always true -- this flag was needed when integrated in the same GUI...
		}
		
}
