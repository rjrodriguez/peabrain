/**
 * Filename: StructuralMarkingBound.java
 * Date: January, 2014 -- first release
 * 
 * Sets values of params for StructuralEnablingBound module.
 * 
 * See StructuralEnablingBoundStrategy.java
 * @author (C) Ivan Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.cli.submodules;

import pipe.modules.bounds.cli.MyParamValue;
import pipe.modules.bounds.cli.PeabraiNCLI;
import pipe.modules.bounds.strategies.*;

public class StructuralMarkingBound extends PeabraiNCLI {
		
		public StructuralMarkingBound(){
			super();
		}		
		
		public void buildParamValueVector(){
			myParamValue = new MyParamValue[3+1];
			myParamValue[3] = new MyParamValue("-p","Place","Place",true,new Integer(0),false,2);
		}

		@Override
		protected void createStrategy() {
			strategy = new StructuralIterator(pnModel);
			((StructuralIterator)strategy).setStructuralType(StructuralType.STRUCTURAL_MARKING);
			((StructuralIterator)strategy).computeOneItem(Integer.parseInt(this.myParamValue[3].getValue().toString()));
		}
}
