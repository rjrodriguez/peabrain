/**
 * Filename: PerformanceEstimation.java
 * Date: January, 2014 -- first release
 * 
 * Sets values of params for PerformanceEstimation module.
 * 
 * See PerformanceEstimationStrategy.java
 * @author (C) Ivan Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.cli.submodules;

import pipe.modules.bounds.cli.MyParamValue;
import pipe.modules.bounds.cli.PeabraiNCLI;
import pipe.modules.bounds.strategies.*;

public class PerformanceEstimation extends PeabraiNCLI {
		
		public PerformanceEstimation(){
			super();
		}		
		
		public void buildParamValueVector(){
			myParamValue = new MyParamValue[3+1];
			myParamValue[3] = new MyParamValue("-e","epsilon","full description",false,new Double(0.0),false,2);
		}

		@Override
		public void createStrategy() {	
			strategy = new PerformanceEstimationStrategy(pnModel);
			//Cast
			((PerformanceEstimationStrategy) strategy).setParameterEpsilon( Double.parseDouble( this.myParamValue[3].getValue().toString() ) );
		}
}
