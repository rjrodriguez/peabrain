/**
 * Filename: ResourceOptimisation.java
 * Date: January, 2014 -- first release
 * 
 * Sets values of params for ResourceOptomisation module.
 * 
 * See ResourceOptimisationStrategy.java
 * @author (C) Ivan Pamplona (University of Zaragoza, 2014) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */

package pipe.modules.bounds.cli.submodules;
import pipe.modules.bounds.cli.MyParamValue;
import pipe.modules.bounds.cli.PeabraiNCLI;
import pipe.modules.bounds.strategies.*;

public class ResourceOptimisation extends PeabraiNCLI {
		
		public ResourceOptimisation(){
			super();
		}		
		
		public void buildParamValueVector(){
			myParamValue = new MyParamValue[3+3];
			myParamValue[3] = new MyParamValue("-b","budget","budget",false,new Double(0),false,2);
			myParamValue[4] = new MyParamValue("-p","process","budget",false,new String(),false,2);
			myParamValue[5] = new MyParamValue("-cr","cost-resource (Ej. -cr [1001:0,1002:5]","cost-resource (Ej. -cr [1001:0,1002:5]",false,new String(),false,2);
		}

		protected void createStrategy() {
			strategy = new ResourceOptimisationStrategy(pnModel);

			String resocost = this.myParamValue[5].getValue().toString();
			resocost = resocost.replace("[", "");
			resocost = resocost.replace("]", "");
			System.out.println(resocost);
			
			String [] resocostString = resocost.toString().split(",");
			double[] cost= new double[resocostString.length];
			int[] resource= new int[resocostString.length];
			
			for (int i=0; i<resocostString.length; i++){
				String [] resocostString2 = resocostString[i].toString().split(":");
				resource[i] = Integer.parseInt(resocostString2[1]);
				cost[i] = Double.parseDouble(resocostString2[0]);
			}
		
			((ResourceOptimisationStrategy) strategy).setParametersBudget ( Double.parseDouble( this.myParamValue[3].getValue().toString() ) );
			((ResourceOptimisationStrategy) strategy).setParametersProcessIdle(this.pnModel.getPlaceID(Integer.parseInt(this.myParamValue[4].getValue().toString())));
			((ResourceOptimisationStrategy) strategy).setParametersIdxResources(resource);
			((ResourceOptimisationStrategy) strategy).setParametersCostVector(cost);
		}
}
