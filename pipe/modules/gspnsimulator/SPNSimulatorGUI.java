/**
 * Filename: SimulatorGUI.java
 * Date: January, 2012 -- first release
 * Creates a GUI for collecting information needed for simulating a Petri net
 * 
 * See Simulator.java
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.gspnsimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import javax.swing.filechooser.FileFilter;
import javax.swing.JPanel;
import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JTextArea;


import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.views.PetriNetView;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class SPNSimulatorGUI extends JFrame {
	private static final long serialVersionUID = -3763499294966658950L;
	private JPanel pnlLoadFile;
	private JButton btnLoadPnmlFile;
	private JScrollPane scrollPaneDebug;
	private JTextArea txtDebugInfo;
	private JPanel panel;
	private SimulatorGUI pnlCentral;

	public SPNSimulatorGUI()
	{
		redirectSystemStreams();
		
		setBounds(100, 100, 690, 550);
		setTitle("PeabraiN GSPNSimulator GUI - alpha release");
		getContentPane().setLayout(new BorderLayout(0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		// Center & show
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		pnlLoadFile = new JPanel();
		getContentPane().add(pnlLoadFile, BorderLayout.NORTH);
		pnlLoadFile.setLayout(new BoxLayout(pnlLoadFile, BoxLayout.X_AXIS));
		
		btnLoadPnmlFile = new JButton("Load PNML file");
		btnLoadPnmlFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadPNMLfile();				
			}
		});
		
		pnlLoadFile.add(btnLoadPnmlFile, BorderLayout.WEST);
		
		panel = new JPanel();
		pnlLoadFile.add(panel, BorderLayout.NORTH);
		
		scrollPaneDebug = new JScrollPane();
		JLabel lblInfo = new JLabel("Debug info:");
		
		txtDebugInfo = new JTextArea();
		txtDebugInfo.setRows(5);
		scrollPaneDebug.setViewportView(txtDebugInfo);
		txtDebugInfo.setEditable(false);
		txtDebugInfo.setBounds(0,  0, 100, 300);
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(lblInfo, BorderLayout.NORTH);
		panel.add(scrollPaneDebug, BorderLayout.CENTER);
		
		pnlCentral = new SimulatorGUI(null);
		getContentPane().add(pnlCentral.getContentPane(), BorderLayout.CENTER);
		pnlCentral.setVisible(false);
		
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private void loadPNMLfile()
	{
		//Create a file chooser
		final JFileChooser fc = new JFileChooser();
		fc.addChoosableFileFilter(new XMLFileFilter());
		//In response to a button click:
		int retVal = fc.showOpenDialog(this);
		if(retVal == JFileChooser.APPROVE_OPTION)
		{
			String loadedFile = fc.getSelectedFile().getName();
			txtDebugInfo.append("Loading file: [" + loadedFile + "]...");
		
			// Load data panel
		
			// Create GUI from PeabraiN simulator and get component pane
			PetriNetModel pnModel = new PetriNetModel(new PetriNetView(fc.getSelectedFile().getAbsolutePath()));
			pnlCentral.setPNModel(pnModel);
		}
	}
		
	class XMLFileFilter extends FileFilter
	{
		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) {
		        return true;
		    }

			int dotposition= f.getName().lastIndexOf(".");
			String extension = f.getName().substring(dotposition + 1, f.getName().length()); 
		    if (extension != null) {
		        if (extension.equals("xml"))
		        {
		                return true;
		        }else
		        {
		            return false;
		        }
		    }

		    return false;
		}

		@Override
		public String getDescription() {
			return "PNML files (.xml)";
		}
		
	}
	
    private void updateTextArea(String valueOf) {
    	txtDebugInfo.append(valueOf);
	}
	
	private void redirectSystemStreams() {  
		  OutputStream out = new OutputStream() {  
		    @Override  
		    public void write(int b) throws IOException {  
		      updateTextArea(String.valueOf((char) b));  
		    }  
		  
			@Override  
		    public void write(byte[] b, int off, int len) throws IOException {  
		      updateTextArea(new String(b, off, len));  
		    }  
		  
		    @Override  
		    public void write(byte[] b) throws IOException {  
		      write(b, 0, b.length);  
		    }  
		  };  
		  
		  System.setOut(new PrintStream(out, true));  
		  System.setErr(new PrintStream(out, true));  
		}  
}