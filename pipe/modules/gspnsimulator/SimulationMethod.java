/**
 * Filename: SimulationMethod.java
 * Date: January, 2012 -- first release
 * 
 * Enumeration for distinguishing kind of transitions in the Petri net model
 *
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>. 
 */
package pipe.modules.gspnsimulator;

public enum SimulationMethod {
	EXACT_SIM("Exact"),
	APPROX_SIM("Approximate");
	
	private String value;

	SimulationMethod(String value) {
        this.value = value;
    }
	
	public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
