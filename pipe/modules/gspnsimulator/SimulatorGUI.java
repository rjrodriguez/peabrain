/**
 * Filename: SimulatorGUI.java
 * Date: January, 2012 -- first release
 * Creates a GUI for collecting information needed for simulating a Petri net
 * 
 * See Simulator.java
 * 
 * @author (C) Ricardo J. Rodríguez (University of Zaragoza, 2011) 
 *
 * This file is part of PeabraiN.
 *
 * PeabraiN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PeabraiN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with PeabraiN.  If not, see <http://www.gnu.org/licenses/>.
 */
package pipe.modules.gspnsimulator;

import java.awt.Dimension;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import pipe.modules.bounds.dataLayer.PetriNetModel;
import pipe.modules.bounds.gui.PeabraiNGUI;

import javax.swing.JFormattedTextField;
import javax.swing.table.DefaultTableModel;

public class SimulatorGUI extends PeabraiNGUI {
	private static final long serialVersionUID = -3763499294966658950L;
	
	private JFormattedTextField txtConfLevel, 
			txtSimTime, txtErrorAccuracy,
			txtMaxRepetitions, txtMinRepetitions;
	private JTable table;
	private MyTableModel tableModel;
	private JScrollPane scrollPane_2;
	private JComboBox cmbSimMethod;

	public SimulatorGUI(Simulator s)
	{
		super(s.getCurrentPNModel(), true, s);
		btnCompute.setEnabled(isComputeButtonActive());
	}
	
	protected void enableComputeButton() {
		this.btnCompute.setEnabled(
				(isComputeButtonActive()));
	}

	private boolean isComputeButtonActive()
	{
		return pnModel != null && 
				!(pnModel.getPlacesSize() == 0 || pnModel.getTransitionsSize() == 0);
	}
	
	protected void compute()
	{	
		double 	simTime = -1,
				confLevel,
				errorAccuracy;
		int		minRepetitions = -1, 
				maxRepetitions = minRepetitions;
		
		setText("Computing...");
		repaint();
		
		try{
			//System.out.println(this.txtSimTime.getText());
			if(this.txtSimTime.getText().length() != 0)
				simTime = Double.parseDouble(this.txtSimTime.getText());
		}
		catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(this, "Error on simulation time. Please check input values.", 
					"ERROR on simulation parameters", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		try{
			confLevel = Double.parseDouble(this.txtConfLevel.getText());
			if(confLevel >= 100 || confLevel <= 0)
				throw new NumberFormatException();
		}
		catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(this, "Error on simulation confidence level. Please check input values.", 
					"ERROR on simulation parameters", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		try{
			errorAccuracy = Double.parseDouble(this.txtErrorAccuracy.getText());
			if(errorAccuracy >= 100 || errorAccuracy <= 0)
				throw new NumberFormatException();
		}
		catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(this, "Error on accuracy. Please check input values.", 
					"ERROR on simulation parameters", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		/* Get num of min repetitions */
		try{
			if(this.txtMinRepetitions.getText().length() != 0)
				minRepetitions = Integer.parseInt(this.txtMinRepetitions.getText());
			
			if(this.txtMinRepetitions.getText().length() != 0 && 
					minRepetitions <= 1)
				throw new Exception();
		}
		catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(this, "Error on number of min. repetitions. Please check input values.", 
					"ERROR on simulation parameters", JOptionPane.ERROR_MESSAGE);
			return;
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Error on number of min. repetitions. Minimum MUST BE GREATER THAN ONE.\nPlease check input values.", 
					"ERROR on simulation parameters", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		/* Get num of max repetitions */
		try{
			if(this.txtMaxRepetitions.getText().length() != 0)
				maxRepetitions = Integer.parseInt(this.txtMaxRepetitions.getText());
			
			/* Check constraint on min, max repetitions */
			if(this.txtMaxRepetitions.getText().length() != 0 && 
					minRepetitions > maxRepetitions)
				throw new Exception();
		}
		catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(this, "Error on number of max. repetitions. Please check input values.", 
					"ERROR on simulation parameters", JOptionPane.ERROR_MESSAGE);
			return;
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Error on number of repetitions. Minimum CANNOT be greater than maximum.\nPlease check input values.", 
					"ERROR on simulation parameters", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		/* Get selected transitions... */
		int[] selectedTransitions = {0}; // Set transition idx 0 by default
		
		if(table.getSelectedRows().length != 0)
			selectedTransitions = table.getSelectedRows(); 
		
		Simulator simulator = SPNSimulator.createSimulator(SimulationMethod.values()[cmbSimMethod.getSelectedIndex()], 
															pnModel);
		
		/* Simulate... */
		simulator.simulate(selectedTransitions, minRepetitions, maxRepetitions, 
								simTime, confLevel/100, errorAccuracy/100);
		//simulator.simulate(maxSimulation);
		
		/* Get results */
		setText(simulator.printResult());
	}
	

	@Override
	protected void createAdditionalGUIElements() {
		pnlData.setPreferredSize(new Dimension(450, 275));
		pnlData.setLayout(null);
		
		JLabel lblMaxSimulationTime = new JLabel("Max. sim. time (u.t.):");
		lblMaxSimulationTime.setBounds(12, 69, 150, 25);
		pnlData.add(lblMaxSimulationTime);
		
		txtSimTime = new JFormattedTextField(NumberFormat.getNumberInstance());
		txtSimTime.setBounds(162, 69, 67, 25);
		pnlData.add(txtSimTime);
		
		JLabel lblConfidenceLevel = new JLabel("Confidence level (%):");
		lblConfidenceLevel.setBounds(12, 106, 157, 25);
		pnlData.add(lblConfidenceLevel);
		
		txtConfLevel = new JFormattedTextField(NumberFormat.getNumberInstance());
		txtConfLevel.setBounds(162, 106, 36, 25);
		txtConfLevel.setText("95");
		pnlData.add(txtConfLevel);
		
		JLabel lblError = new JLabel("Error (%):");
		lblError.setBounds(12, 138, 66, 25);
		pnlData.add(lblError);
		
		txtErrorAccuracy = new JFormattedTextField(NumberFormat.getNumberInstance());
		txtErrorAccuracy.setBounds(162, 138, 36, 25);
		txtErrorAccuracy.setText("5");
		pnlData.add(txtErrorAccuracy);
		
		JLabel lbMaxReplications = new JLabel("Max. no. repetitions:");
		lbMaxReplications.setBounds(12, 32, 150, 25);
		pnlData.add(lbMaxReplications);
		
		txtMaxRepetitions = new JFormattedTextField(new DecimalFormat("#"));
		txtMaxRepetitions.setBounds(162, 32, 56, 25);
		pnlData.add(txtMaxRepetitions);
		
		JLabel lblNoIterations = new JLabel("Min. no. repetitions:");
		lblNoIterations.setBounds(12, 5, 150, 25);
		pnlData.add(lblNoIterations);
		
		txtMinRepetitions = new JFormattedTextField(new DecimalFormat("#"));
		txtMinRepetitions.setBounds(162, 5, 57, 25);
		pnlData.add(txtMinRepetitions);
		
		
		JLabel lblTransitionToObserve = new JLabel("Transitions/places to observe:");
		lblTransitionToObserve.setBounds(241, 5, 200, 25);
		pnlData.add(lblTransitionToObserve);
		
		/* Set transition selection table */
		table = new JTable(tableModel = new MyTableModel());
		tableModel.setDataVector(getItemsData(), getColumnsName());
		//table.setBounds(12, 87, 219, 128);
		//getContentPane().add(table);
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(251, 36, 181, 228);
		pnlData.add(scrollPane_2);
		scrollPane_2.setViewportView(table);
		
		// Add selection of simulation method
		JLabel lblSimMethod = new JLabel("Simulation Method:");
		lblSimMethod.setBounds(12, 166, 157, 25);
		pnlData.add(lblSimMethod);

		cmbSimMethod = new JComboBox(getSimulationMethods());
		//comboBox = new JComboBox<String>();
		/*cmbSimMethod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});*/
		cmbSimMethod.setBounds(162, 166, 85, 25);
		pnlData.add(cmbSimMethod);
	}
	
	private String[] getSimulationMethods()
	{
		// Create a string to store simulation methods
		String[] simMethods = new String[SimulationMethod.values().length];
		
		for(int i = 0; i < simMethods.length; i++){
	        simMethods[i] = SimulationMethod.values()[i].toString();
	    }
		
		return simMethods;
	}
	
	private String[] getColumnsName()
	{
		String[] aux = {"Transition/Place"};
		return aux;
	}
	
	private String[][] getItemsData() {
		/* Create a string for each place/transition */
		
		try{
			String[][] vStrings = null;
			
					vStrings = new String[this.pnModel.getTransitionsSize() + this.pnModel.getPlacesSize()][1];
					for(int i = 0; i < this.pnModel.getTransitionsSize(); i++)
						vStrings[i][0] = this.pnModel.getTransitionID(i);
					int aux = this.pnModel.getTransitionsSize();
					for(int i = 0; i < this.pnModel.getPlacesSize(); i++)
						vStrings[i + aux][0] = this.pnModel.getPlaceID(i);
					
			return vStrings;
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	private class MyTableModel extends DefaultTableModel
	{
		private static final long serialVersionUID = 6739152974904569148L;

		public boolean isCellEditable (int row, int column)
		   {
		       return false;
		   }
	}

	@Override
	protected String getGUIName() {
		return "GSPN Simulation Analysis";
	}
	
	public void setPNModel(PetriNetModel pnModel)
	{
		this.pnModel = pnModel;
		/* Recompute table model */
		tableModel.setDataVector(getItemsData(), getColumnsName());
		enableComputeButton();
	}

}